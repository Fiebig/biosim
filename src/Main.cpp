/*
 * Main.cpp
 *
 *  Created on: 18.03.2016
 *      Author: Alex
 */

#include "./frontend/BioSim.h"


wxIMPLEMENT_APP_NO_MAIN(biosim::BioSim);


#if defined WIN32 && !defined DEBUG
//winmain doesnt allocate a console
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow) {
	//init and set app instance
	wxApp* pApp = new biosim::BioSim();
	wxApp::SetInstance(pApp);
	//wxWidgets initialisation + enter mainloop
	return wxEntry(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}

#else
int main(int argc, char** argv) {
	//init and set app instance
	wxApp* pApp = new biosim::BioSim();
	wxApp::SetInstance(pApp);
	//wxWidgets initialisation + enter mainloop
	return wxEntry(argc, argv);
}
#endif