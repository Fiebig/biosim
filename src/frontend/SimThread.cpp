/*
 * SimThread.cpp
 *
 *  Created on: 01.09.2015
 *      Author: Alex
 */
#include "SimThread.h"

#include <chrono>

#include "SimWindow.h"

namespace biosim {

	SimThread::SimThread(SimWindow* simWindow__, unsigned sleeptime__) :
		wxThread(wxTHREAD_JOINABLE), m_simWindow(simWindow__), m_isInterrupted(
			false), m_sleeptime(sleeptime__) {

	}

	SimThread::~SimThread() {
	}

	void SimThread::Interrupt() {
		if (IsAlive())
			m_isInterrupted.store(true);
	}

	wxThread::ExitCode SimThread::Entry() {
		while (true) {
			std::chrono::milliseconds time_span;
			{
				//acquire lock
				wxMutexLocker lock(m_simWindow->m_mutexThreadloop);
				//stop if pause flag is set
				if (m_simWindow->m_simPaused.load())
					m_simWindow->m_simResumed.Wait();
				//breaks loop if interrupted flag is set
				if (m_isInterrupted.load())
					break;
				//
				auto start = std::chrono::steady_clock::now();
				// Make SimStep
				m_simWindow->SimStep();
				// Update SimWindow
				// QueueEvent() takes control of the pointer and deletes it
				wxQueueEvent(m_simWindow,
					new wxThreadEvent(wxEVT_SIMTHREAD_UPDATE));
				//
				auto end = std::chrono::steady_clock::now();
				time_span =
					std::chrono::duration_cast<std::chrono::milliseconds>(
						end - start);
				//release lock
			}
			int sleepInMs = std::max(
				(int)(m_sleeptime.load() - time_span.count()), 10);
			//Sleep
			wxThread::This()->Sleep(sleepInMs);
		}
		//Success
		return (wxThread::ExitCode) 0;
	}

}/*namespace biosim*/
