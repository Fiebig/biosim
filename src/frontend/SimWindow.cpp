/*
 * SimWindow.cpp
 *
 *  Created on: 11.08.2015
 *      Author: Alex
 */
#include "SimWindow.h"

#include <cmath>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "../backend/TerrainMap.h"
#include "../backend/Tile.h"
#include "../backend/Creature.h"
#include "AppResources.h"
#include "BioSim.h"
#include "SimThread.h"

namespace biosim {

    wxDEFINE_EVENT(wxEVT_SIMTHREAD_UPDATE, wxThreadEvent);


    SimWindow::SimWindow() :
        wxScrolledWindow(), m_simThread(new SimThread(this)), m_mutexMap(
            wxMUTEX_DEFAULT), m_mutexThreadloop(wxMUTEX_DEFAULT), m_simResumed(
            m_mutexThreadloop), m_simPaused(true), m_activeTile(nullptr) {
    }

    SimWindow::SimWindow(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                         const wxSize& size, LandKarte* karte__) :
        wxScrolledWindow(parent, id, pos, size,
#ifdef WIN32
                        wxHSCROLL | wxVSCROLL | wxALWAYS_SHOW_SB
#else
                        wxHSCROLL | wxVSCROLL
#endif
        ), m_terrainMap(karte__), m_simThread(
            new SimThread(this)), m_mutexMap(wxMUTEX_DEFAULT), m_mutexThreadloop(
            wxMUTEX_DEFAULT), m_simResumed(m_mutexThreadloop), m_simPaused(
            true), m_activeTile(nullptr) {

        m_activeTile = &(*m_terrainMap)(0,0);
        //connect event to a method
        Bind(wxEVT_SIMTHREAD_UPDATE, &SimWindow::OnThreadUpdate, this);
        UpdateScrollAndSize(m_ppUScroll, m_ppUScroll);
    }

    SimWindow::~SimWindow() {
        //nonblocking in oppposite to Delete() function
        m_simThread->Interrupt();
        //resume thread if paused so that Entry() can finish
        if (m_simPaused.load()) {
            m_simPaused.store(false);
            m_simResumed.Signal();
        }
        //wait for thread completion if it was started
        if (m_simThread->IsAlive())
            m_simThread->Wait();
        //joinable thread must be deleted manually
        delete m_simThread;
        //
        Unbind(wxEVT_SIMTHREAD_UPDATE, &SimWindow::OnThreadUpdate, this);
    }

    BEGIN_EVENT_TABLE(SimWindow, wxScrolledWindow)
        //
        EVT_PAINT(SimWindow::OnPaint)
        EVT_ERASE_BACKGROUND(SimWindow::OnErase)
        EVT_SCROLLWIN(SimWindow::OnScroll)
        EVT_LEFT_DOWN(SimWindow::OnLeftClick)
        EVT_MOUSEWHEEL(SimWindow::OnMouseWheel)
        EVT_KEY_DOWN(SimWindow::OnKeyDown)
        EVT_SIZE(SimWindow::OnSize)
        //
    END_EVENT_TABLE()

    bool SimWindow::Create(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                           const wxSize& size, LandKarte* karte__) {
        // superclass create
        if (!wxScrolledWindow::Create(parent, id, pos, size,
#ifdef WIN32
            wxHSCROLL | wxVSCROLL | wxALWAYS_SHOW_SB
#else
            wxHSCROLL | wxVSCROLL
#endif
        ))
            return false;
        //init karte
        m_terrainMap = std::move(std::unique_ptr<LandKarte>(karte__));
        m_activeTile = &(*m_terrainMap)(0,0);
        //connect event to a method
        Bind(wxEVT_SIMTHREAD_UPDATE, &SimWindow::OnThreadUpdate, this);
        UpdateScrollAndSize(m_ppUScroll, m_ppUScroll);
        return true;
    }

    void SimWindow::UpdateScrollAndSize(int ppuX, int ppuY) {
        // init scrolled area size, scrolling speed and max size
        const wxSize& kachelsize = wxGetApp().m_resources->getTilePicSize();
        unsigned w_map_px = m_terrainMap->getWidth() * kachelsize.GetWidth() * m_zoomFactor;
        unsigned h_map_px = m_terrainMap->getHeight() * kachelsize.GetHeight() * m_zoomFactor;
        SetVirtualSize(w_map_px, h_map_px);
        SetScrollRate(ppuX, ppuY);
        SetMaxClientSize(wxSize(w_map_px, h_map_px));
    }

    void SimWindow::GetStartPixelCoord(int& startPxX, int& startPxY) const {
        int ppuX, ppuY, startX, startY, virtX, virtY;
        GetScrollPixelsPerUnit(&ppuX, &ppuY);
        GetViewStart(&startX, &startY);//in scroll units
        GetVirtualSize(&virtX, &virtY);//in pixels
        int maxX = virtX - GetClientSize().x;
        int maxY = virtY - GetClientSize().y;
        startPxX = std::min(ppuX * startX, maxX);
        startPxY = std::min(ppuY * startY, maxY);
    }

    LandKarte& SimWindow::getTerrainMap() {
        return *m_terrainMap;
    }

    void SimWindow::SetMap(LandKarte* karte__) {
        wxMutexLocker lock(m_mutexThreadloop);
        m_activeTile = nullptr;
        {
            wxMutexLocker lock(m_mutexMap);
            //release old karte
            delete m_terrainMap.release();
            //erase Creatures
            Creature::s_allCreatures.clear();
            //init new karte
            m_terrainMap = std::move(std::unique_ptr<LandKarte>(karte__));
        }
        ClearBackground();
        //
        m_activeTile = &(*m_terrainMap)(0,0);
        // init scrolled area size, scrolling speed and max size
        UpdateScrollAndSize(m_ppUScroll, m_ppUScroll);
        //redraw
        Refresh();
        Update();
    }

    void SimWindow::ClearMap() {
        wxMutexLocker lock(m_mutexMap);
        m_terrainMap->clearMap();
        Creature::s_allCreatures.clear();
    }

    void SimWindow::SimStep() {
        wxMutexLocker lock(m_mutexMap);
        //Make aktion for each creature
        for (auto it = Creature::s_allCreatures.begin();
             it != Creature::s_allCreatures.end();) {
            //
            Creature* cr = it->get();
            if (cr->isAlive() || cr->getDeadCount() < 10) {
                cr->nextAction(*m_terrainMap);
                ++it;
            }
            else {
                cr->getCurrentTile()->removeCreature(
                      const_cast<const Creature*>(cr));
                it = Creature::s_allCreatures.erase(it);
            }
        }
    }

    bool SimWindow::PlaceCreature(Creature* cr, Kachel& tile) {
        wxMutexLocker lock(m_mutexMap);
        //Stelle sicher, dass Kreatur nur dort platziert wird
        //wo sie sich auch aufhalten kann
        if (cr->canBeOnTerrain(tile.getType())) {
            cr->setCurrentTile(tile);
            std::shared_ptr<Creature> shcr(cr);
            cr->getCurrentTile()->addCreature(shcr);
            Creature::s_allCreatures.push_back(std::shared_ptr<Creature>(shcr));
            return true;
        }
        return false;
    }

    void SimWindow::SetSimSpeed(unsigned speedInMs) {
        m_simThread->m_sleeptime.store(speedInMs);
    }

    bool SimWindow::IsSimPaused() const {
        return m_simPaused.load();
    }

    bool SimWindow::DoStartThread() {
        // Is SimThread already Running?
        if (!m_simThread->IsAlive()) {
            m_simPaused.store(false);
            //Start the Thread
            if (m_simThread->Run() != wxTHREAD_NO_ERROR) {
                m_simPaused.store(true);
                return false;
            }
            return true;
        }
        return false;
    }

    bool SimWindow::DoPauseThread() {
        // Is SimThread already Running?
        if (m_simThread->IsAlive()) {
            m_simPaused.store(true);
            return true;
        }
        return false;
    }

    bool SimWindow::DoResumeThread() {
        // Is SimThread already Running?
        if (m_simThread->IsAlive()) {
            m_simPaused.store(false);
            m_simResumed.Signal();
            return true;
        }
        return false;
    }

    //Event Handlers
    //###############################################################################

    void SimWindow::OnThreadUpdate(wxThreadEvent& event) {
        Refresh();
        wxFrame* mainFrame = static_cast<wxFrame*>(GetGrandParent());
        mainFrame->SetStatusText(
            "CreatureCount: "
            + std::to_string(Creature::s_allCreatures.size()), 2);
    }

    void SimWindow::OnSize(wxSizeEvent& event) {
        const wxSize& kachelsize = wxGetApp().m_resources->getTilePicSize();
        double w_map_px = m_terrainMap->getWidth() * kachelsize.GetWidth();
        double h_map_px = m_terrainMap->getHeight() * kachelsize.GetHeight();
        m_minZoomFactor = std::max(GetClientSize().x / w_map_px, GetClientSize().y / h_map_px);
        Refresh();
        event.Skip();
    }

    void SimWindow::OnLeftClick(wxMouseEvent& event) {
        //note must use wxClientDC because we are outside of OnPaint()
        wxClientDC dc(this);
        //compute current startposition in pixels
        int startPxX, startPxY;
        GetStartPixelCoord(startPxX, startPxY);
        //set DC orgin vgl. DoPrepareDC
        dc.SetDeviceOrigin(-startPxX, -startPxY);
        //set zoom
        dc.SetUserScale(m_zoomFactor, m_zoomFactor);
        //calculate tile position
        const wxPoint& pos = event.GetLogicalPosition(dc);
        const wxSize& kachelsize = wxGetApp().m_resources->getTilePicSize();
        int xMap = pos.x / kachelsize.GetWidth();
        int yMap = pos.y / kachelsize.GetHeight();
        //set active tile
        m_activeTile = &(*m_terrainMap)(xMap, yMap);
#ifdef DEBUG
        std::cout << "CursorTile:" << std::endl;
        std::cout << xMap << ',' << yMap << std::endl;
#endif
        Refresh();
        event.Skip();
    }

    void SimWindow::OnMouseWheel(wxMouseEvent& event) {
        // update zoom and virtual size
        double old_zoom_factor = m_zoomFactor;
        int wheelRot = event.GetWheelRotation();
        m_zoomFactor = (wheelRot > 0) ? m_zoomFactor + m_zoomSpeed :
                           m_zoomFactor - m_zoomSpeed;
        m_zoomFactor = std::min(m_zoomFactor, m_maxZoomFactor);
        m_zoomFactor = std::max(m_zoomFactor, m_minZoomFactor);
        UpdateScrollAndSize(m_ppUScroll, m_ppUScroll);
        // compute anti-scroll
        int startScrX, startScrY, virtX, virtY, ppuX, ppuY;
        GetViewStart(&startScrX, &startScrY);//in scroll units
        GetVirtualSize(&virtX, &virtY);//in pixels
        GetScrollPixelsPerUnit(&ppuX, &ppuY);
        const wxSize& kachelsize = wxGetApp().m_resources->getTilePicSize();
        int xMinLand, xMaxLand, yMinLand, yMaxLand;
        ComputeRenderCoord(kachelsize, startScrX * ppuX, startScrY * ppuY,
                           xMinLand, xMaxLand, yMinLand, yMaxLand);
        double tileAddX = std::min(std::max((m_activeTile->getX()- xMinLand)/ (double)(xMaxLand-xMinLand), 0.0), 1.0);
        double tileAddY = std::min(std::max((m_activeTile->getY()- yMinLand)/ (double)(yMaxLand-yMinLand), 0.0), 1.0);
        //
        double tileOldX = ((m_activeTile->getX()+tileAddX) * kachelsize.GetX()*old_zoom_factor) / ppuX;
        double tileOldY = ((m_activeTile->getY()+tileAddY) * kachelsize.GetY()*old_zoom_factor) / ppuY;
        double tileX = ((m_activeTile->getX()+tileAddX) * kachelsize.GetX()*m_zoomFactor) / ppuX;
        double tileY = ((m_activeTile->getY()+tileAddY) * kachelsize.GetY()*m_zoomFactor) / ppuY;
        double shiftX = abs(tileOldX - startScrX);
        double shiftY = abs(tileOldY - startScrY);
        double normS = sqrt(shiftX*shiftX + shiftY*shiftY);
        shiftX /= normS;
        shiftY /= normS;
        double normT = sqrt((tileX-tileOldX)*(tileX-tileOldX) + (tileY-tileOldY)*(tileY-tileOldY));
        shiftX *= normT;
        shiftY *= normT;
        int scrollX = ceil(shiftX);
        int scrollY = ceil(shiftY);
        //
        int maxX = (virtX - GetClientSize().x) / (double)ppuX;
        int maxY = (virtY - GetClientSize().y) / (double)ppuY;
        if(wheelRot > 0){
            Scroll(std::min(startScrX + scrollX, maxX), std::min(startScrY + scrollY, maxY));
        }
        else {
            Scroll(std::max(startScrX - scrollX, 0), std::max(startScrY - scrollY, 0));
        }
        //
        Refresh();
    }


    void SimWindow::OnKeyDown(wxKeyEvent& event) {
        //compute current startposition in scroll units
        int startScrX, startScrY, virtX, virtY, ppuX, ppuY;
        GetViewStart(&startScrX, &startScrY);//in scroll units
        GetVirtualSize(&virtX, &virtY);//in pixels
        GetScrollPixelsPerUnit(&ppuX, &ppuY);
        int maxX = (virtX - GetClientSize().x) / (double)ppuX;
        int maxY = (virtY - GetClientSize().y) / (double)ppuY;
        //scroll per unit is m_ppUScroll pixels
        int units = 5 * m_zoomFactor;
        //
        int keycode = event.GetKeyCode();
        switch (keycode) {
        case WXK_LEFT: case 'A': case 'a':
            Scroll(std::max(startScrX - units, 0), startScrY);
            break;
        case WXK_RIGHT: case 'D': case 'd':
            Scroll(std::min(startScrX + units, maxX), startScrY);
            break;
        case WXK_DOWN: case 'S': case 's':
            Scroll(startScrX, std::min(startScrY + units, maxY));
            break;
        case WXK_UP: case 'W': case'w':
            Scroll(startScrX, std::max(startScrY - units, 0));
            break;
        default:
            event.Skip();
            break;
        }
        //
        Refresh();
    }


    void SimWindow::OnErase(wxEraseEvent& event) {
        //Empty so no flickering during scroll
    }

    void SimWindow::OnScroll(wxScrollWinEvent& event) {
        Refresh();
        event.Skip();
    }

    void SimWindow::OnPaint(wxPaintEvent& event) {
        // Create device context
        wxPaintDC dc(this);
        //compute current startposition in pixels
        int startPxX, startPxY;
        GetStartPixelCoord(startPxX, startPxY);
        //set DC orgin vgl. DoPrepareDC
        dc.SetDeviceOrigin(-startPxX, -startPxY);
        //set zoom
        dc.SetUserScale(m_zoomFactor, m_zoomFactor);
        //declare vars + get tilesize
        int xMinLand, xMaxLand, yMinLand, yMaxLand;
        const wxSize& tilesize = wxGetApp().m_resources->getTilePicSize();
        //acquire lock
        wxMutexLocker lock(m_mutexMap);
        //compute startpoints + endpoints of visible Map in tilecoord
        ComputeRenderCoord(tilesize, startPxX, startPxY, xMinLand, xMaxLand,
                           yMinLand, yMaxLand);
#ifdef DEBUG
        std::cout << "RenderRect:" << std::endl;
        std::cout << xMinLand << ',' << yMinLand << ',' << xMaxLand << ','
            << yMaxLand << std::endl;
#endif
        //Draw ONLY VISIBLE Map
        RenderMap(dc, tilesize, xMinLand, xMaxLand, yMinLand, yMaxLand);
    }


    void SimWindow::ComputeRenderCoord(const wxSize& tilesize, int startPxX,
                                       int startPxY, int& xMinLand, int& xMaxLand, int& yMinLand,
                                       int& yMaxLand) const {
        double tsw = tilesize.GetWidth()* m_zoomFactor;
        double tsh = tilesize.GetHeight()* m_zoomFactor;
        // compute startpoints of visible Map in tilecoord (round down)
        xMinLand = startPxX / tsw;
        yMinLand = startPxY / tsh;
        //compute window offsets in pixels
        const wxSize& currentWindowSize = this->GetClientSize();
        int window_offsetX = std::min(currentWindowSize.GetWidth(),
                                      (int)(tsw * m_terrainMap->getWidth()));
        int window_offsetY = std::min(currentWindowSize.GetHeight(),
                                      (int)(tsh * m_terrainMap->getHeight()));
        //compute endpoints of visible Map in tilecoord  (round down)
        xMaxLand = (startPxX + window_offsetX) / tsw;
        yMaxLand = (startPxY + window_offsetY) / tsh;
        //check boundaries
        if (xMinLand < 0)
            xMinLand = 0;
        if (xMaxLand >= (int)m_terrainMap->getWidth())
            xMaxLand = m_terrainMap->getWidth() - 1;
        if (yMinLand < 0)
            yMinLand = 0;
        if (yMaxLand >= (int)m_terrainMap->getHeight())
            yMaxLand = m_terrainMap->getHeight() - 1;
    }


    void SimWindow::RenderMap(wxDC& dc, const wxSize& tilesize,
                              int xMinLand, int xMaxLand, int yMinLand, int yMaxLand) {
        //Get tilepics
        const std::vector<wxBitmap>& terrainbitmaps =
            wxGetApp().m_resources->getTerrainPics();
        //Draw ONLY VISIBLE Map
        for (int i = yMinLand; i <= yMaxLand; i++)
            for (int j = xMinLand; j <= xMaxLand; j++) {
                Kachel& kt = (*m_terrainMap)(j, i);
                // get bitmap with enum index (Reihenfolge in bitmaps muss stimmen!!!)
                const wxBitmap& terr_bmp = terrainbitmaps[kt.getType()];
                //draw terrain bitmap
                dc.DrawBitmap(terr_bmp, j * tilesize.GetWidth(),
                              i * tilesize.GetHeight(), terr_bmp.HasAlpha());
                //draw creatures
                for (const std::shared_ptr<Creature>& cr : kt.getTilecreatures()) {
                    const wxBitmap* cr_bmp =
                        (cr->isAlive()) ?
                            wxGetApp().m_resources->getCreaturePic(
                                cr->m_typeData) :
                            wxGetApp().m_resources->getDeadPic();
                    dc.DrawBitmap(*cr_bmp, j * tilesize.GetWidth(),
                                  i * tilesize.GetHeight(), cr_bmp->HasAlpha());
                }
                //draw cursor
                if (&kt == m_activeTile)
                    RenderCursor(dc, tilesize, *m_activeTile);
            }
    }


    void SimWindow::RenderCursor(wxDC& dc, const wxSize& tilesize, const Kachel& cursor_tile) {
        const wxBitmap& cursorbmp = wxGetApp().m_resources->getCursorPic();
        dc.DrawBitmap(cursorbmp, cursor_tile.getX() * tilesize.GetWidth(),
            cursor_tile.getY() * tilesize.GetHeight(), cursorbmp.HasAlpha());
    }


}/*namespace biosim*/
