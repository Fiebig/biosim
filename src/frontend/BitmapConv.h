/*
 * BitmapConv.h
 *
 *  Created on: 14.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef FRONTEND_BITMAPCONV_H_
#define FRONTEND_BITMAPCONV_H_

class wxBitmap;

namespace biosim {

	class TgaPicture;

	//puts the data stored in a TgaPicture to a wxBitmap for rendering
	wxBitmap TgatoBitmap(const TgaPicture& pic);

}/*namespace biosim*/

#endif /* FRONTEND_BITMAPCONV_H_ */
