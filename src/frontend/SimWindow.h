/*
 * SimWindow.h
 *
 *  Created on: 11.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef FRONTEND_SIMWINDOW_H_
#define FRONTEND_SIMWINDOW_H_

#include <memory>
#include <atomic>
#include <wx/scrolwin.h>

class wxBitmap;
class wxPaintDC;
class wxClientDC;


namespace biosim {

	class LandKarte;
	class Kachel;
	class Creature;
	class SimThread;
	// declare a new type of event, to be used by SimThread class:
	wxDECLARE_EVENT(wxEVT_SIMTHREAD_UPDATE, wxThreadEvent);

    /**
     * @brief The SimWindow class
     */
    class SimWindow: public wxScrolledWindow {
		// allow SimThread to access mutexes and conditions
		friend class SimThread;
    private:
		std::unique_ptr<LandKarte> m_terrainMap;
		//joinable thread must be deleted manually
		SimThread* m_simThread;
		// mutexes for Map- and ThreadIteration Protection
		wxMutex m_mutexMap, m_mutexThreadloop;
		// Condition to ensure that Simulation stopps only after
		// a complete SimStep
		wxCondition m_simResumed;
		std::atomic<bool> m_simPaused;
		//values for zoom and scroll
        double m_zoomFactor{1.0};
		double m_minZoomFactor{1.0};
		const double m_zoomSpeed{0.05f};
        const double m_maxZoomFactor{2.0};
		const int m_ppUScroll{5};
	public:
		//no ownership
		Kachel* m_activeTile;
		SimWindow();
		SimWindow(wxWindow* parent, wxWindowID id, const wxPoint& pos,
		          const wxSize& size, LandKarte* karte);
		~SimWindow();
		//two step construction
		bool Create(wxWindow* parent, wxWindowID id, const wxPoint& pos,
		            const wxSize& size, LandKarte* karte);
		LandKarte& getTerrainMap();
		//reset the map
		void SetMap(LandKarte* karte__);
		//clear current map from creatures
		void ClearMap();
		//each creature makes a step
		void SimStep();
		bool PlaceCreature(Creature* cr, Kachel& tile);
		void SetSimSpeed(unsigned speedInMs);
		bool IsSimPaused() const;
		bool DoStartThread();
		bool DoPauseThread();
		bool DoResumeThread();
	private:
		void UpdateScrollAndSize(int ppuX, int ppuY);
		void GetStartPixelCoord(int& startPxX, int& startPxY) const;
		//callback for event from SimThread
		void OnThreadUpdate(wxThreadEvent& event);
		void OnSize(wxSizeEvent& event);
		void OnScroll(wxScrollWinEvent& event);
		void OnErase(wxEraseEvent& event);
		void OnLeftClick(wxMouseEvent& event);
		void OnMouseWheel(wxMouseEvent& event);
		void OnKeyDown(wxKeyEvent& event);
		void OnPaint(wxPaintEvent& event);
		//rendering stuff
		void ComputeRenderCoord(const wxSize& tilesize, int startPxX,
		                        int startPxY, int& xMinLand, int& xMaxLand, int& yMinLand,
		                        int& yMaxLand) const;
		void RenderMap(wxDC& dc, const wxSize& tilesize, int xMinLand,
		               int xMaxLand, int yMinLand, int yMaxLand);
		void RenderCursor(wxDC& dc, const wxSize& tilesize, const Kachel& cursor_tile);
		//
		wxDECLARE_EVENT_TABLE();
	};

}/*namespace biosim*/

#endif /* FRONTEND_SIMWINDOW_H_ */
