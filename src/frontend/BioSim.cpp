/*
 * BioSim.cpp
 *
 *  Created on: 12.08.2015
 *      Author: Alex
 */

#include "BioSim.h"

#include "../backend/TerrainMap.h"
#include "MainFrame.h"
#include "AppResources.h"

namespace biosim {

	bool BioSim::OnInit() {
		try {
			const char* crdatapath = (argc > 1) ? argv[1].mb_str() : "./resources/creaturedata.txt";
			const char* terrainpathes[6] =
					{ "./resources/terrain/deep_sea.tga",
							"./resources/terrain/shallow_water.tga",
							"./resources/terrain/sand.tga",
							"./resources/terrain/earth.tga",
							"./resources/terrain/rocks.tga",
							"./resources/terrain/snow.tga" };
			const char* cursorpath = "./resources/other/cursor.tga";
			const char* deadpath = "./resources/other/dead.tga";
			m_resources = AppResources::load(crdatapath, terrainpathes, 6,
					cursorpath, deadpath);
		} catch (const std::exception& e) {
			std::string msg("Fehler beim Laden der Resourcen: ");
			msg.append(e.what());
			wxLogError(msg.c_str());
			return false;
		}
		MainFrame *frame = new MainFrame("BioSim", wxPoint(50, 50),
				wxSize(1200, 675), new LandKarte(32, 32));
		frame->Show(true);
		SetTopWindow(frame);
		return true;
	}

	int BioSim::OnExit() {
		delete m_resources;
		return 0;
	}

}/*namespace biosim*/

