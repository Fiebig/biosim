/*
 * AppResources.cpp
 *
 *  Created on: 12.08.2015
 *      Author: Alex
 */

#include "AppResources.h"

#include "../backend/IO_CrData.h"
#include "../backend/IO_TgaPic.h"
#include "BitmapConv.h"

namespace biosim {

	AppResources::AppResources(std::vector<KreaturTypData> creaturedata__,
	                           std::vector<wxBitmap> creaturepics__,
	                           std::vector<wxBitmap> terrainpics__,
	                           std::unique_ptr<wxBitmap> cursorpic__,
	                           std::unique_ptr<wxBitmap> deadpic__) :
		m_creaturedata(std::move(creaturedata__)), m_creaturepics(
			std::move(creaturepics__)), m_terrainpics(
			std::move(terrainpics__)), m_cursorpic(
			std::move(cursorpic__)), m_pictureMap(
			creaturedata__.size()), m_deadpic(std::move(deadpic__)) {

		for (unsigned i = 0; i < m_creaturedata.size(); i++)
			m_pictureMap.emplace(&m_creaturedata[i], &m_creaturepics[i]);
	}

	const std::vector<KreaturTypData>& AppResources::getCreatureData() const {
		return m_creaturedata;
	}

	const std::vector<wxBitmap>& AppResources::getTerrainPics() const {
		return m_terrainpics;
	}

	wxSize AppResources::getTilePicSize() const {
		return m_terrainpics.back().GetSize();
	}

	const std::vector<wxBitmap>& AppResources::getCreaturePics() const {
		return m_creaturepics;
	}

	const wxBitmap& AppResources::getCursorPic() const {
		return *m_cursorpic;
	}

	const wxBitmap* AppResources::getDeadPic() const {
		return m_deadpic.get();
	}

	const wxBitmap* AppResources::getCreaturePic(
		const KreaturTypData* crtyp) const {
		auto it = m_pictureMap.find(crtyp);
		if (it != m_pictureMap.end())
			return it->second;
		else
			return nullptr;
	}

	AppResources* AppResources::load(const char* creaturepath,
	                                 const char** terrainpathes, unsigned anz_tp, const char* cursorpath,
	                                 const char* deadpath) {
		// Kreaturendefinition einlesen
		const std::vector<KreaturTypData>& creaturedata = readCreatureTypes(
			creaturepath);
		// Kreaturenbilder laden
		std::vector<wxBitmap> creaturepics;
		for (const KreaturTypData& cr : creaturedata) {
			const TgaPicture& cr_tgapic = TgaPicture::load(
				cr.m_picturePath.c_str());
			creaturepics.push_back(TgatoBitmap(cr_tgapic));
		}
		// Terrainbilder laden
		std::vector<wxBitmap> terrainpics;
		for (unsigned i = 0; i < anz_tp; i++) {
			const TgaPicture& terr_tgapic = TgaPicture::load(terrainpathes[i]);
			terrainpics.push_back(TgatoBitmap(terr_tgapic));
		}
		//Cursorbild laden
		const TgaPicture& cursor_tgapic = TgaPicture::load(cursorpath);
		std::unique_ptr<wxBitmap> cursorbmp(
			new wxBitmap(TgatoBitmap(cursor_tgapic)));

		//Deadbild laden
		const TgaPicture& dead_tgapic = TgaPicture::load(deadpath);
		std::unique_ptr<wxBitmap> deadbmp(
			new wxBitmap(TgatoBitmap(dead_tgapic)));
		//Construct
		return new AppResources(std::move(creaturedata),
		                        std::move(creaturepics), std::move(terrainpics),
		                        std::move(cursorbmp), std::move(deadbmp));
	}

}/*namespace biosim*/
