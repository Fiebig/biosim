/*
 * BitMapConv.cpp
 *
 *  Created on: 14.08.2015
 *      Author: Alex
 */
#include "BitmapConv.h"

#include <stdexcept>
#include <wx/rawbmp.h>

#include "../backend/IO_TgaPic.h"

namespace biosim {

	static wxBitmap TgatoBitmap_RGBA(const TgaPicture& pic) {
		wxBitmap bitmap(pic.width(), pic.height(), 32);
		if (!bitmap.IsOk())
			throw std::runtime_error("Conversion to wxBitmap failed");

		wxAlphaPixelData bmdata(bitmap);
		if (!bmdata)
			throw std::runtime_error("Conversion to wxBitmap failed");

		wxAlphaPixelData::Iterator dst(bmdata);
		const unsigned char* rgba = pic.getRawData();
		//correct y-flip
		for (int y = pic.height() - 1; y >= 0; y--) {
			dst.MoveTo(bmdata, 0, y);
			for (unsigned x = 0; x < pic.width(); x++) {
				//order BGRA
				// wxBitmap contains rgb values pre-multiplied with alpha
				unsigned char a = rgba[3];
				dst.Red() = rgba[2] * a / 255;
				dst.Green() = rgba[1] * a / 255;
				dst.Blue() = rgba[0] * a / 255;
				dst.Alpha() = a;
				++dst;
				rgba += 4;
			}
		}
		return bitmap;
	}

	static wxBitmap TgatoBitmap_RGB(const TgaPicture& pic) {
		wxBitmap bitmap(pic.width(), pic.height(), 24);
		if (!bitmap.IsOk())
			throw std::runtime_error("Conversion to wxBitmap failed");

		wxNativePixelData bmdata(bitmap);
		if (!bmdata)
			throw std::runtime_error("Conversion to wxBitmap failed");

		wxNativePixelData::Iterator dst(bmdata);
		const unsigned char* rgb = pic.getRawData();
		//correct y-flip
		for (int y = pic.height() - 1; y >= 0; y--) {
			dst.MoveTo(bmdata, 0, y);
			for (unsigned x = 0; x < pic.width(); x++) {
				//order BGR
				dst.Red() = rgb[2];
				dst.Green() = rgb[1];
				dst.Blue() = rgb[0];
				++dst;
				rgb += 3;
			}
		}
		return bitmap;
	}

	wxBitmap TgatoBitmap(const TgaPicture& pic) {
		if (pic.getHeader().bitpropixel == 32)
			return TgatoBitmap_RGBA(pic);
		else
			return TgatoBitmap_RGB(pic);
	}

}/*namespace biosim*/
