/*
 * MainFrame.cpp
 *
 *  Created on: 08.08.2015
 *      Author: Alex
 */
#include "MainFrame.h"

#include <random>
#include <chrono>
#include <algorithm>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "../backend/Creature.h"
#include "../backend/TerrainMap.h"
#include "../backend/Tile.h"
#include "AppResources.h"
#include "BioSim.h"
#include "SimWindow.h"

namespace biosim {

	MainFrame::MainFrame(const wxString& title, const wxPoint& pos,
		const wxSize& size, LandKarte* startkarte) :
		wxFrame(nullptr, wxID_ANY, title, pos, size) {
		CreateMenuANDStatus();
		CreateClientArea(startkarte);
		InitCreatureCB();
	}

	wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
		//
		EVT_CLOSE(MainFrame::OnClose)
		EVT_MENU(wxID_EXIT, MainFrame::OnExit)
		EVT_MENU(wxID_ABOUT, MainFrame::OnAbout)
		EVT_MENU(ID_RandomPopulate, MainFrame::OnRandomPopulate)
		EVT_MENU(ID_ClearMap, MainFrame::OnClearMap)
		EVT_MENU(ID_MenuStartSim, MainFrame::OnButtonStart)
		EVT_MENU(ID_MenuStopSim, MainFrame::OnButtonPause)
		EVT_SIZE(MainFrame::OnSize)
		EVT_BUTTON(ID_PlatzierenBtn, MainFrame::OnButtonPlatzieren)
		EVT_BUTTON(ID_GenMapBtn, MainFrame::OnButtonGenMap)
		EVT_BUTTON(ID_StartBtn, MainFrame::OnButtonStart)
		EVT_BUTTON(ID_PauseBtn, MainFrame::OnButtonPause)
		EVT_BUTTON(ID_SchrittBtn, MainFrame::OnButtonSchritt)
		EVT_CHOICE(ID_CreatureCB, MainFrame::OnCreatureChoice)
		EVT_SLIDER(ID_SpeedSlider, MainFrame::OnSlide)
		//
		wxEND_EVENT_TABLE()

	
	void MainFrame::OnClose(wxCloseEvent& event) {
		event.SetCanVeto(false);
		Destroy();
	}

	void MainFrame::OnExit(wxCommandEvent& event) {
		Close(true);
	}

	void MainFrame::OnAbout(wxCommandEvent& event) {
		wxMessageDialog aboutDialog(this,
			"BioSim ist eine kleine interaktive Biologiesimulation, "
			"in der im Rahmen einer Sandkastenumgebung ein funktionierendes �kosystem aus "
			"verscheidenen Kreaturentypen (z.B. Gras, Obstb�ume, K�he, Fische oder Tiger) "
			"zusammengestellt werden kann.\n"
			"Dazu bietet die Simulation dem Anwender die M�glichkeit, "
			"aus einer vorgegebenen Liste Kreaturen auszuw�hlen. "
			"Anschliessend kann der Nutzer die Kreaturen nach Belieben auf einer virtuellen, "
			"zuf�llig generierten Umgebungskarte platzieren. "
			"Weitere Schaltfl�chen starten oder pausieren die Simulation.\n"
			"W�hrend dem Simulationsablauf bewegen sich die Kreaturen "
			"entsprechend den Regeln einer einfachen KI. "
			"Pflanzen verbreiten sich beispielsweise, sofern gen�gend Raum im Umkreis vorhanden ist. "
			"Tiere wandern umher und ruhen sich daraufhin aus, "
			"oder suchen Nahrung, um ihren Hunger zu stillen. "
			"Eine begrenzte Lebenszeit erfordert schliesslich die Partnersuche zur Fortpflanzung.",
			"About BioSim",
			wxOK | wxICON_INFORMATION);
		aboutDialog.ShowModal();
	}

	void MainFrame::OnRandomPopulate(wxCommandEvent& event) {
		if (m_simWindow->IsSimPaused()) {
			//random TileData creation
			std::vector<Kachel>& mapData =
				m_simWindow->getTerrainMap().getMapData();
			std::vector<Kachel*> randomTiles(mapData.size());
			for (unsigned i = 0; i < randomTiles.size(); i++)
				randomTiles[i] = &mapData[i];
			std::mt19937 engine(
				std::chrono::high_resolution_clock::now().time_since_epoch().count());
			std::shuffle(randomTiles.begin(), randomTiles.end(), engine);
			//random CreatureData creation
			const std::vector<KreaturTypData>& creatureData =
				wxGetApp().m_resources->getCreatureData();
			std::uniform_int_distribution<unsigned> distrib(0,
				creatureData.size() - 1);
			unsigned creatureCount = mapData.size() / 10;
			std::vector<const KreaturTypData*> randomCrData(creatureCount);
			for (unsigned i = 0; i < creatureCount; i++)
				randomCrData[i] = &creatureData[distrib(engine)];
			//creature placement
			for (unsigned i = 0; i < creatureCount; i++) {
				Creature* cr = ConstructCreature(randomCrData[i], nullptr);
				unsigned tileIndex = 0;
				while (true) {
					if (tileIndex >= randomTiles.size()) {
						delete cr;
						break;
					}
					if (randomTiles[tileIndex]->creatureCount() == 0
						&& m_simWindow->PlaceCreature(cr,
							*randomTiles[tileIndex]))
						break;
					tileIndex++;
				}
			}
			//window update
			m_simWindow->Refresh();
			m_simWindow->Update();
			SetStatusText(
				"CreatureCount: "
				+ std::to_string(Creature::s_allCreatures.size()),
				2);
		}
	}

	void MainFrame::OnClearMap(wxCommandEvent& event) {
		if (m_simWindow->IsSimPaused()) {
			m_simWindow->ClearMap();
			m_simWindow->Refresh();
			m_simWindow->Update();
			SetStatusText(
				"CreatureCount: "
				+ std::to_string(Creature::s_allCreatures.size()),
				2);
		}
	}

	void MainFrame::OnSize(wxSizeEvent& event) {
		event.Skip();
	}

	void MainFrame::OnSlide(wxCommandEvent& event) {
		m_simWindow->SetSimSpeed(m_speedSlider->GetValue());
	}

	void MainFrame::OnButtonPlatzieren(wxCommandEvent& event) {
		//Check if there is an active tile
		if (m_simWindow->m_activeTile != nullptr) {
			//construct creature based on cb selection
			const KreaturTypData* cr_data =
				static_cast<const KreaturTypData*>(m_creatureCB->GetClientData(
					m_creatureCB->GetCurrentSelection()));
			Creature* cr = ConstructCreature(cr_data, nullptr);
			//if possible, place creature on active tile, else delete creature
			if (m_simWindow->PlaceCreature(cr, *m_simWindow->m_activeTile)) {
				m_simWindow->Refresh();
				SetStatusText(
					"CreatureCount: "
					+ std::to_string(
						Creature::s_allCreatures.size()), 2);
			}
			else {
				delete cr;
			}
		}
	}

	Creature* MainFrame::ConstructCreature(const KreaturTypData* cr_data,
		Kachel* current_tile) const {
		std::string name(cr_data->m_name);
		Creature* cr;
		if (name == "Algen")
			cr = new Algen(cr_data, current_tile);
		else if (name == "Seetang")
			cr = new Seetang(cr_data, current_tile);
		else if (name == "Plankton")
			cr = new Plankton(cr_data, current_tile);
		else if (name == "Gras")
			cr = new Gras(cr_data, current_tile);
		else if (name == "Kaktus")
			cr = new Kaktus(cr_data, current_tile);
		else if (name == "Gebuesch")
			cr = new Gebuesch(cr_data, current_tile);
		else if (name == "Sonnenblume")
			cr = new Sonnenblume(cr_data, current_tile);
		else if (name == "Eiche")
			cr = new Eiche(cr_data, current_tile);
		else if (name == "Obstbaum")
			cr = new Obstbaum(cr_data, current_tile);
		else if (name == "Tannenbaum")
			cr = new Tannenbaum(cr_data, current_tile);
		else if (name == "Wels")
			cr = new Wels(cr_data, current_tile);
		else if (name == "Forelle")
			cr = new Forelle(cr_data, current_tile);
		else if (name == "Krabbe")
			cr = new Krabbe(cr_data, current_tile);
		else if (name == "Hai")
			cr = new Hai(cr_data, current_tile);
		else if (name == "Delphin")
			cr = new Delphin(cr_data, current_tile);
		else if (name == "Kuh")
			cr = new Kuh(cr_data, current_tile);
		else if (name == "Pferd")
			cr = new Pferd(cr_data, current_tile);
		else if (name == "Emu")
			cr = new Emu(cr_data, current_tile);
		else if (name == "Schaf")
			cr = new Schaf(cr_data, current_tile);
		else if (name == "Hund")
			cr = new Hund(cr_data, current_tile);
		else if (name == "Baer")
			cr = new Baer(cr_data, current_tile);
		else
			cr = new Tiger(cr_data, current_tile);
		return cr;
	}

	void MainFrame::OnButtonGenMap(wxCommandEvent& event) {
		if (m_simWindow->IsSimPaused()) {
			unsigned mapWidth = wxAtoi(m_mapWidthCB->GetStringSelection());
			unsigned mapHeight = wxAtoi(m_mapHeightCB->GetStringSelection());
			m_simWindow->SetMap(new LandKarte(mapWidth, mapHeight));
			SetStatusText(
				"Map: " + std::to_string(mapWidth) + "x"
				+ std::to_string(mapHeight), 0);
			SetStatusText("CreatureCount: 0", 2);
			SetMaxClientSize(
				wxSize(
					5 + m_simWindow->GetMaxWidth()
					+ m_sizerRightArea->GetMinSize().x,
					std::max(m_simWindow->GetMaxHeight(),
						m_sizerRightArea->GetMinSize().y)));
			//
			if (m_simWindow->GetMaxHeight() > m_sizerRightArea->GetMinSize().y) {
				int sizediffY = GetMaxSize().y - GetMaxClientSize().y;
				SetMaxSize(wxSize(GetMaxSize().x, GetMaxSize().y - sizediffY));
			}
			m_main_panel->Layout();
			this->Fit();
		}
	}

	void MainFrame::OnButtonStart(wxCommandEvent& event) {
		if (!m_simWindow->DoStartThread())
			m_simWindow->DoResumeThread();
		SetStatusText("SimState: Running", 1);
	}

	void MainFrame::OnButtonPause(wxCommandEvent& event) {
		m_simWindow->DoPauseThread();
		SetStatusText("SimState: Stopped", 1);
	}

	void MainFrame::OnButtonSchritt(wxCommandEvent& event) {
		if (m_simWindow->IsSimPaused()) {
			m_simWindow->SimStep();
			m_simWindow->Refresh();
			m_simWindow->Update();
			SetStatusText(
				"CreatureCount: "
				+ std::to_string(Creature::s_allCreatures.size()),
				2);
		}
	}

	void MainFrame::OnCreatureChoice(wxCommandEvent& event) {
		const KreaturTypData* cr =
			static_cast<const KreaturTypData*>(m_creatureCB->GetClientData(
				m_creatureCB->GetCurrentSelection()));
		m_stField->ChangeValue(wxString::Format(wxT("%u"), cr->m_strength));
		m_geschField->ChangeValue(wxString::Format(wxT("%u"), cr->m_speed));
		m_lebenField->ChangeValue(wxString::Format(wxT("%u"), cr->m_life));
		m_eigenField->ChangeValue(cr->m_characteristics.c_str());
	}

	//Layout
	//##################################################################################

	void MainFrame::CreateMenuANDStatus() {
		wxMenu* menuFile = new wxMenu;
		menuFile->AppendSeparator();
		menuFile->Append(ID_MenuStartSim, "Start");
		menuFile->Append(ID_MenuStopSim, "Stop");
		menuFile->Append(ID_RandomPopulate, "Random Populate");
		menuFile->Append(ID_ClearMap, "Clear Map");
		menuFile->Append(wxID_EXIT);
		wxMenu* menuHelp = new wxMenu;
		menuHelp->Append(wxID_ABOUT);
		wxMenuBar* menuBar = new wxMenuBar;
		menuBar->Append(menuFile, "&Simulation");
		menuBar->Append(menuHelp, "&Help");
		SetMenuBar(menuBar);
		CreateStatusBar(3);
		SetStatusText("Map: 32x32", 0);
		SetStatusText("SimState: Stopped", 1);
		SetStatusText("CreatureCount: 0", 2);
	}

	void MainFrame::CreateClientArea(LandKarte* karte__) {
		// Create main panel and sizer for it
		m_main_panel = new wxPanel(this, ID_MainPanel);
		wxBoxSizer* sizerTopLevel = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer* sizerMainLayout = new wxBoxSizer(wxHORIZONTAL);
		//Create SimWindow
		m_simWindow = new SimWindow(m_main_panel, ID_SimBereich,
			wxDefaultPosition, wxSize(600, 600), karte__);
		// Create righthandside Area + Sizer
		m_sizerRightArea = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer* creatureBox = CreateCreatureBox(m_main_panel);
		wxBoxSizer* mapBox = CreateMapBox(m_main_panel);
		wxBoxSizer* controlBox = CreateControlBox(m_main_panel);
		//Fill sizers
		m_sizerRightArea->Add(creatureBox, 0, wxEXPAND | wxALIGN_TOP | wxTOP,
			10);
		m_sizerRightArea->Add(mapBox, 0, wxEXPAND | wxALIGN_TOP | wxTOP, 10);
		m_sizerRightArea->Add(0, 0, wxEXPAND);
		m_sizerRightArea->Add(controlBox, 0,
			wxEXPAND | wxTOP | wxBOTTOM, 10);
		//note proportion 0 and 1
		sizerMainLayout->Add(m_simWindow, 1, wxEXPAND | wxLEFT, 5);
		sizerMainLayout->Add(m_sizerRightArea, 0, wxEXPAND);
		sizerTopLevel->Add(sizerMainLayout, 1, wxEXPAND);
		//Set sizer and max size
		m_main_panel->SetSizerAndFit(sizerTopLevel);
		SetMaxClientSize(
			wxSize(
				5 + m_simWindow->GetMaxWidth()
				+ m_sizerRightArea->GetMinSize().x,
				std::max(m_simWindow->GetMaxHeight(),
					m_sizerRightArea->GetMinSize().y)));
		//
		if (m_simWindow->GetMaxHeight() > m_sizerRightArea->GetMinSize().y) {
			int sizediffY = GetMaxSize().y - GetMaxClientSize().y;
			SetMaxSize(wxSize(GetMaxSize().x, GetMaxSize().y - sizediffY));
		}
		this->Fit();
	}

	wxBoxSizer* MainFrame::CreateCreatureBox(wxWindow* parent) {
		// Create Creature Box + Sizer
		wxStaticBox* creatureBox = new wxStaticBox(parent, wxID_ANY,
			wxT("Kreaturenwahl"), wxDefaultPosition, wxSize(240, 300));
		wxStaticBoxSizer* sizerCreatureBox = new wxStaticBoxSizer(creatureBox,
			wxVERTICAL);
		// Create creature attribute sizer
		wxBoxSizer *sizerStData = new wxBoxSizer(wxHORIZONTAL),
			*sizerSpeedData = new wxBoxSizer(wxHORIZONTAL), *sizerLifeData =
			new wxBoxSizer(wxHORIZONTAL), *sizerCharisticsData =
			new wxBoxSizer(wxHORIZONTAL);
		//Create attribute labels + data fields
		wxStaticText *stLabel = new wxStaticText(creatureBox, wxID_ANY,
			wxT("Staerke:")), *geschLabel = new wxStaticText(creatureBox,
				wxID_ANY, wxT("Geschw.:")), *lebenLabel = new wxStaticText(
					creatureBox, wxID_ANY, wxT("Leben:")), *eigenLabel =
			new wxStaticText(creatureBox, wxID_ANY, wxT("Eigensch.:"));
		m_stField = new wxTextCtrl(creatureBox, ID_StaerkeField, wxT("##"),
			wxDefaultPosition, wxSize(70, 25),
			wxTE_READONLY | wxTE_CENTRE);
		m_geschField = new wxTextCtrl(creatureBox, ID_GeschField, wxT("##"),
			wxDefaultPosition, wxSize(70, 25),
			wxTE_READONLY | wxTE_CENTRE);
		m_lebenField = new wxTextCtrl(creatureBox, ID_LebenField, wxT("##"),
			wxDefaultPosition, wxSize(70, 25),
			wxTE_READONLY | wxTE_CENTRE);
		m_eigenField = new wxTextCtrl(creatureBox, ID_EigenField, wxT("##"),
			wxDefaultPosition, wxSize(160, 70),
			wxTE_MULTILINE | wxTE_READONLY | wxTE_LEFT);
		//Fill Sizers
		sizerStData->Add(stLabel, 0, wxALIGN_LEFT | wxLEFT, 40);
		sizerStData->Add(0, 0, wxEXPAND);
		sizerStData->Add(m_stField, 0, wxRIGHT, 100);
		sizerSpeedData->Add(geschLabel, 0, wxALIGN_LEFT | wxLEFT, 40);
		sizerSpeedData->Add(0, 0, wxEXPAND);
		sizerSpeedData->Add(m_geschField, 0, wxRIGHT, 100);
		sizerLifeData->Add(lebenLabel, 0, wxALIGN_LEFT | wxLEFT, 40);
		sizerLifeData->Add(0, 0, wxEXPAND);
		sizerLifeData->Add(m_lebenField, 0, wxRIGHT, 100);
		sizerCharisticsData->Add(eigenLabel, 0, wxALIGN_LEFT | wxLEFT, 40);
		sizerCharisticsData->Add(0, 0, wxEXPAND);
		sizerCharisticsData->Add(m_eigenField, 0, wxRIGHT, 60);
		// Create a choice box inside the static box sizer
		m_creatureCB = new wxChoice(creatureBox, ID_CreatureCB,
			wxDefaultPosition, wxSize(150, 35));
		// Create platzieren button
		wxButton* platzierenBtn = new wxButton(creatureBox, ID_PlatzierenBtn,
			wxT("Platzieren"));
		// Create Creature Box Content
		sizerCreatureBox->Add(m_creatureCB, 0,
			wxALIGN_CENTER_HORIZONTAL | wxALL, 10);
		sizerCreatureBox->Add(sizerStData, 0, wxEXPAND | wxTOP | wxBOTTOM, 10);
		sizerCreatureBox->Add(sizerSpeedData, 0, wxEXPAND | wxTOP | wxBOTTOM,
			10);
		sizerCreatureBox->Add(sizerLifeData, 0, wxEXPAND | wxTOP | wxBOTTOM,
			10);
		sizerCreatureBox->Add(sizerCharisticsData, 0,
			wxEXPAND | wxTOP | wxBOTTOM, 10);
		sizerCreatureBox->Add(platzierenBtn, 0, wxALIGN_RIGHT | wxALL, 10);
		return sizerCreatureBox;
	}

	void MainFrame::InitCreatureCB() {
		//Add CreatureData to ChoiceBox
		for (const KreaturTypData& cr : wxGetApp().m_resources->getCreatureData())
			m_creatureCB->Append(cr.m_name.c_str(), (void *)(&cr));
		//Set Initial Selection
		m_creatureCB->SetSelection(0);
		const KreaturTypData* cr =
			static_cast<const KreaturTypData*>(m_creatureCB->GetClientData(
				0));
		m_stField->ChangeValue(wxString::Format(wxT("%u"), cr->m_strength));
		m_geschField->ChangeValue(wxString::Format(wxT("%u"), cr->m_speed));
		m_lebenField->ChangeValue(wxString::Format(wxT("%u"), cr->m_life));
		m_eigenField->ChangeValue(cr->m_characteristics.c_str());
	}

	wxBoxSizer* MainFrame::CreateMapBox(wxWindow* parent) {
		// Create Map Box + Sizers
		wxStaticBox* mapBox = new wxStaticBox(parent, wxID_ANY,
			wxT("Kartengenerierung"), wxDefaultPosition, wxSize(240, 100));
		wxStaticBoxSizer* sizerMapBox = new wxStaticBoxSizer(mapBox,
			wxVERTICAL);
		wxBoxSizer* sizerSizeBtns = new wxBoxSizer(wxHORIZONTAL);
		//Create ChoiceBoxes + Button
		wxStaticText* widthLabel = new wxStaticText(mapBox, wxID_ANY,
			wxT("Breite:"));
		m_mapWidthCB = new wxChoice(mapBox, ID_MapWidthCB, wxDefaultPosition,
			wxSize(60, 30));
		for (unsigned i = 16; i <= 256; i *= 2)
			m_mapWidthCB->Append(wxString::Format(wxT("%u"), i));
		m_mapWidthCB->SetSelection(1);
		wxStaticText* heightLabel = new wxStaticText(mapBox, wxID_ANY,
			wxT("Hoehe:"));
		m_mapHeightCB = new wxChoice(mapBox, ID_MapHeightCB, wxDefaultPosition,
			wxSize(60, 30));
		for (unsigned i = 16; i <= 256; i *= 2)
			m_mapHeightCB->Append(wxString::Format(wxT("%u"), i));
		m_mapHeightCB->SetSelection(1);
		wxButton* genMapBtn = new wxButton(mapBox, ID_GenMapBtn,
			wxT("Karte erstellen"));
		//Fill sizers
		sizerSizeBtns->Add(widthLabel, 0, wxALIGN_LEFT | wxLEFT, 10);
		sizerSizeBtns->Add(m_mapWidthCB, 0, wxLEFT, 10);
		sizerSizeBtns->Add(heightLabel, 0, wxLEFT | wxRIGHT, 10);
		sizerSizeBtns->Add(m_mapHeightCB, 0, wxRIGHT, 10);
		sizerMapBox->Add(sizerSizeBtns, 0, wxALIGN_CENTER_HORIZONTAL | wxALL,
			10);
		sizerMapBox->Add(genMapBtn, 0, wxALIGN_RIGHT | wxALL, 10);
		return sizerMapBox;
	}

	wxBoxSizer* MainFrame::CreateControlBox(wxWindow* parent) {
		// Create Control Box + Sizers
		wxStaticBox* controlBox = new wxStaticBox(parent, wxID_ANY,
			wxT("Simulationssteuerung"), wxDefaultPosition,
			wxSize(240, 100));
		wxStaticBoxSizer* sizerControlBox = new wxStaticBoxSizer(controlBox,
			wxVERTICAL);
		wxBoxSizer* sizerControlBtns = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer* sizerSlider = new wxBoxSizer(wxHORIZONTAL);
		//Create Control Buttons
		wxButton* startBtn = new wxButton(controlBox, ID_StartBtn,
			wxT("Start"));
		wxButton* pauseBtn = new wxButton(controlBox, ID_PauseBtn,
			wxT("Pause"));
		wxButton* schrittBtn = new wxButton(controlBox, ID_SchrittBtn,
			wxT("Schritt"));
		//Create SpeedSlider + Label
		wxStaticText* sliderLabel = new wxStaticText(controlBox, wxID_ANY,
			wxT("Simgeschw.:"));
		m_speedSlider = new wxSlider(controlBox, ID_SpeedSlider, 1000, 200,
			2000, wxDefaultPosition, wxSize(200, 40),
			wxSL_HORIZONTAL | wxSL_MIN_MAX_LABELS);
		//Fill Sizers
		sizerControlBtns->Add(startBtn, 0, wxALIGN_LEFT | wxALL, 10);
		sizerControlBtns->Add(0, 0, wxEXPAND);
		sizerControlBtns->Add(pauseBtn, 0, wxALL,
			10);
		sizerControlBtns->Add(0, 0, wxEXPAND);
		sizerControlBtns->Add(schrittBtn, 0, wxALL, 10);
		sizerSlider->Add(sliderLabel, 0, wxALIGN_LEFT | wxLEFT, 10);
		sizerSlider->Add(m_speedSlider, 0, wxLEFT,
			10);
		sizerControlBox->Add(sizerControlBtns, 0, wxALIGN_CENTER_HORIZONTAL);
		sizerControlBox->Add(sizerSlider, 0, wxALIGN_LEFT | wxTOP, 20);
		return sizerControlBox;
	}

}/*namespace biosim*/
