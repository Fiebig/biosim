/*
 * BioSim.h
 *
 *  Created on: 12.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef FRONTEND_BIOSIM_H_
#define FRONTEND_BIOSIM_H_

#include <wx/app.h>

namespace biosim {

	class AppResources;

	class BioSim: public wxApp {
	public:
		AppResources* m_resources;
		bool OnInit() override;
		int OnExit() override;
	};

}/*namespace biosim*/

wxDECLARE_APP(biosim::BioSim);

#endif /* FRONTEND_BIOSIM_H_ */
