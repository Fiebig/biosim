/*
 * AppResources.h
 *
 *  Created on: 12.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef FRONTEND_APPRESOURCES_H_
#define FRONTEND_APPRESOURCES_H_

#include <vector>
#include <unordered_map>
#include <memory>
#include <wx/bitmap.h>

#include "../backend/CreatureData.h"

namespace biosim {

	class AppResources {
		std::vector<KreaturTypData> m_creaturedata;
		std::vector<wxBitmap> m_creaturepics;
		std::vector<wxBitmap> m_terrainpics;
		std::unique_ptr<wxBitmap> m_cursorpic;
		std::unordered_map<const KreaturTypData*, wxBitmap*> m_pictureMap;
		std::unique_ptr<wxBitmap> m_deadpic;
		AppResources(std::vector<KreaturTypData> creaturedata__,
				std::vector<wxBitmap> creaturepics__,
				std::vector<wxBitmap> terrainpics__,
				std::unique_ptr<wxBitmap> cursorpic__,
				std::unique_ptr<wxBitmap> deadpic__);
	public:
		const std::vector<KreaturTypData>& getCreatureData() const;
		const std::vector<wxBitmap>& getCreaturePics() const;
		const std::vector<wxBitmap>& getTerrainPics() const;
		wxSize getTilePicSize() const;
		const wxBitmap& getCursorPic() const;
		const wxBitmap* getDeadPic() const;
		const wxBitmap* getCreaturePic(const KreaturTypData* crtyp) const;
		static AppResources* load(const char* creaturepath,
				const char** terrainpathes, unsigned anz_tp,
				const char* cursorpath, const char* deadpath);
	};

}/*namespace biosim*/

#endif /* FRONTEND_APPRESOURCES_H_ */
