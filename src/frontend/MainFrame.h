/*
 * MainFrame.h
 *
 *  Created on: 07.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef FRONTEND_MAINFRAME_H_
#define FRONTEND_MAINFRAME_H_

#include <wx/frame.h>

class wxChoice;
class wxTextCtrl;
class wxBoxSizer;
class wxSlider;
class wxPanel;

namespace biosim {

	class SimWindow;
	class LandKarte;
	class Creature;
	class KreaturTypData;
	class Kachel;

	class MainFrame: public wxFrame {
		//Automatic Deletion in wxWidgets Window System
		SimWindow* m_simWindow;
		wxChoice* m_creatureCB, *m_mapWidthCB, *m_mapHeightCB;
		wxTextCtrl* m_stField, *m_geschField, *m_lebenField, *m_eigenField;
		wxSlider* m_speedSlider;
		wxBoxSizer* m_sizerRightArea;
        wxPanel* m_main_panel;
	public:
		MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size,
				LandKarte* startkarte);
	private:
		void OnClose(wxCloseEvent& event);
		void OnExit(wxCommandEvent& event);
		void OnAbout(wxCommandEvent& event);
		void OnRandomPopulate(wxCommandEvent& event);
		void OnClearMap(wxCommandEvent& event);
		void OnSize(wxSizeEvent& event);
		void OnSlide(wxCommandEvent& event);
		void OnButtonPlatzieren(wxCommandEvent& event);
		Creature* ConstructCreature(const KreaturTypData* cr_data,
				Kachel* current_tile) const;
		void OnButtonGenMap(wxCommandEvent& event);
		void OnButtonStart(wxCommandEvent& event);
		void OnButtonPause(wxCommandEvent& event);
		void OnButtonSchritt(wxCommandEvent& event);
		void OnCreatureChoice(wxCommandEvent& event);
		//Layout functions
		void CreateMenuANDStatus();
		void CreateClientArea(LandKarte* karte);
		wxBoxSizer* CreateCreatureBox(wxWindow* parent);
		void InitCreatureCB();
		wxBoxSizer* CreateMapBox(wxWindow* parent);
		wxBoxSizer* CreateControlBox(wxWindow* parent);
		//
	wxDECLARE_EVENT_TABLE();
	};

	enum {
		ID_SimBereich = wxID_HIGHEST + 1,
		ID_PlatzierenBtn = wxID_HIGHEST + 2,
		ID_CreatureCB = wxID_HIGHEST + 3,
		ID_StartBtn = wxID_HIGHEST + 4,
		ID_PauseBtn = wxID_HIGHEST + 5,
		ID_SchrittBtn = wxID_HIGHEST + 6,
		ID_StaerkeField = wxID_HIGHEST + 7,
		ID_GeschField = wxID_HIGHEST + 8,
		ID_LebenField = wxID_HIGHEST + 9,
		ID_EigenField = wxID_HIGHEST + 10,
		ID_MainPanel = wxID_HIGHEST + 11,
		ID_SpeedSlider = wxID_HIGHEST + 12,
		ID_MapWidthCB = wxID_HIGHEST + 13,
		ID_MapHeightCB = wxID_HIGHEST + 14,
		ID_GenMapBtn = wxID_HIGHEST + 15,
		ID_RandomPopulate = wxID_HIGHEST + 16,
		ID_ClearMap = wxID_HIGHEST + 17,
		ID_MenuStartSim = wxID_HIGHEST + 18,
		ID_MenuStopSim = wxID_HIGHEST + 19
	};

}/*namespace biosim*/

#endif /* FRONTEND_MAINFRAME_H_ */
