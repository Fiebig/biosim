/*
 * SimThread.h
 *
 *  Created on: 01.09.2015
 *      Author: Alex
 */

#pragma once
#ifndef FRONTEND_SIMTHREAD_H_
#define FRONTEND_SIMTHREAD_H_

#include <atomic>
#include <wx/thread.h>

namespace biosim {

	class SimWindow;

	class SimThread: public wxThread {
		//no ownership
		SimWindow* m_simWindow;
		std::atomic<bool> m_isInterrupted;
	public:
		std::atomic<unsigned> m_sleeptime;
		SimThread(SimWindow* simWindow__, unsigned sleeptime__ = 1000);
		~SimThread();
		void Interrupt();
	private:
		ExitCode Entry() override;
	};

}/*namespace biosim*/

#endif /* FRONTEND_SIMTHREAD_H_ */
