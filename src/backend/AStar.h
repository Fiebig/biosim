/*
 * AStar.h
 *
 *  Created on: 18.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_ASTAR_H_
#define BACKEND_ASTAR_H_

#include <vector>
#include <functional>

namespace biosim {

	class Kachel;
	enum Kacheltyp
	:unsigned;

	typedef std::vector<std::pair<Kachel*, unsigned>> PATHVECTOR;


	unsigned heur_dist_astar(const Kachel& current, const Kachel& target);

	/**
	 * A*-Algorithmus fuer die Wegfindung der Kreaturen.
	 * @param source - Startkachel
	 * @param target - Zielkachel
	 * @param nodes	- Kacheln der Karte
	 * @param wfunc - Gewichtsfunktion fuer die Kanten
	 * @param max_nodes - Obere Schranke der zu betrachtenden Knoten
	 * @return Vector der Knoten von Startkachel zu Zielkachel zusammen mit den jeweiligen Pfadkosten
	 */
	PATHVECTOR AStar(const Kachel& source, const Kachel& target,
	                 const std::vector<Kachel>& nodes,
	                 std::function<int(Kacheltyp)> wfunc, unsigned max_nodes = 50);

}/*namespace biosim*/
#endif /* BACKEND_ASTAR_H_ */
