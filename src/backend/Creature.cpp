/*
 * Creature.cpp
 *
 *  Created on: 16.08.2015
 *      Author: Alex
 */
#include "Creature.h"

#include <typeinfo>
#include <random>
#include <chrono>
#include <algorithm>
#include <iostream>

#include "TerrainMap.h"
#include "Tile.h"
#include "AStar.h"


namespace biosim {

	std::list<std::shared_ptr<Creature>> Creature::s_allCreatures = std::list<
		std::shared_ptr<Creature>>();

	//#####################################################################################
	Creature::Creature(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		m_typeData(type_data__), m_currentLife(type_data__->m_life), m_currentTile(
			current_tile__), m_currentState(WARTEN), m_stepsSincePopulate(
				0), m_deadCounter(0) {
	}

	unsigned Creature::countNeighborsOfSameKind(const LandKarte& terrain_map,
		unsigned dist) const {
		//get borders
		unsigned startX = std::max((int)(m_currentTile->getX() - dist), 0),
			endX = std::min(m_currentTile->getX() + dist,
				terrain_map.getWidth() - 1), startY = std::max(
				(int)(m_currentTile->getY() - dist), 0), endY =
			std::min(m_currentTile->getY() + dist,
				terrain_map.getHeight() - 1);
		//count creatures of same type within region
		//this will be counted too!!!
		unsigned creaturecount = 0;
		for (unsigned i = startY; i <= endY; i++)
			for (unsigned j = startX; j <= endX; j++) {
				const Kachel& k = terrain_map(j, i);
				for (const std::shared_ptr<Creature>& cr : k.getTilecreatures())
					if (cr->isAlive() && cr->getName() == getName()/*typeid(*cr) == typeid(*this)*/)
						creaturecount++;
			}
		return creaturecount;
	}

	std::vector<Kachel*> Creature::searchPopulateTiles(
		const LandKarte& terrain_map, unsigned dist) const {
		//generate options vector
		std::vector<Kachel*>&& options =
			generic_kachel_options(terrain_map, dist,
				[this](const Kachel& kachel)-> bool {
			return canPopulateOn(kachel);
		});
		//shuffel for better random placement
		std::mt19937 engine(
			std::chrono::high_resolution_clock::now().time_since_epoch().count());
		std::shuffle(options.begin(), options.end(), engine);
		return options;
	}

	bool Creature::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		return canBeOnTerrain(kacheltyp);
	}

	bool Creature::canPopulateOn(const Kachel& kachel) const {
		return canPopulateOnTerrain(kachel.getType());
	}

	template <typename T>
	void Creature::generic_populate(
		std::vector<Kachel*>&& kachelOptions) const {
		unsigned childcount = 0;
		//search for preferred regions max 2 new Children
		for (unsigned i = 0; childcount < 2 && i < kachelOptions.size(); i++) {
			Kachel* k = kachelOptions[i];
			if (k->getType() == getPreferredTerrain()) {
				std::shared_ptr<Creature> shcr(new T(m_typeData, k));
				k->addCreature(shcr);
				Creature::s_allCreatures.push_back(
					std::shared_ptr<Creature>(shcr));
				childcount++;
				//efficient hiding (no ownership)
				kachelOptions[i] = nullptr;
			}
		}
		//if less than 2 preferred regions found than fill to 2 new Children
		for (unsigned i = 0; childcount < 2 && i < kachelOptions.size(); i++) {
			Kachel* k = kachelOptions[i];
			if (k != nullptr) {
				std::shared_ptr<Creature> shcr(new T(m_typeData, k));
				k->addCreature(shcr);
				Creature::s_allCreatures.push_back(
					std::shared_ptr<Creature>(shcr));
				childcount++;
			}
		}
	}

	template <typename Func>
	std::vector<Kachel*> Creature::generic_kachel_options(
		const LandKarte& terrain_map, unsigned dist,
		Func&& boolfunc) const {
		//get borders
		unsigned startX = std::max((int)(m_currentTile->getX() - dist), 0),
			endX = std::min(m_currentTile->getX() + dist,
				terrain_map.getWidth() - 1), startY = std::max(
				(int)(m_currentTile->getY() - dist), 0), endY =
			std::min(m_currentTile->getY() + dist,
				terrain_map.getHeight() - 1);
		//generate options vector
		std::vector<Kachel*> options;
		for (unsigned i = startY; i <= endY; i++)
			for (unsigned j = startX; j <= endX; j++) {
				const Kachel& k = terrain_map(j, i);
				if (std::forward<Func>(boolfunc)(k))
					options.push_back(const_cast<Kachel*>(&k));
			}
		return options;
	}

	template <typename Func>
	std::vector<Creature*> Creature::generic_creature_options(
		const LandKarte& terrain_map, unsigned dist,
		Func&& boolfunc) const {
		//get borders
		unsigned startX = std::max((int)(m_currentTile->getX() - dist), 0),
			endX = std::min(m_currentTile->getX() + dist,
				terrain_map.getWidth() - 1), startY = std::max(
				(int)(m_currentTile->getY() - dist), 0), endY =
			std::min(m_currentTile->getY() + dist,
				terrain_map.getHeight() - 1);
		//generate options vector
		std::vector<Creature*> options;
		for (unsigned i = startY; i <= endY; i++)
			for (unsigned j = startX; j <= endX; j++) {
				const Kachel& k = terrain_map(j, i);
				for (const std::shared_ptr<Creature>& cr : k.getTilecreatures()) {
					if (std::forward<Func>(boolfunc)(cr.get()))
						options.push_back(cr.get());
				}
			}
		return options;
	}

	//exor functions
	bool Creature::isTier() const {
		return !isPflanze();
	}

	bool Creature::isWasserbewohner() const {
		return !isLandbewohner();
	}

	//getters + setters
	bool Creature::isAlive() const {
		return m_currentLife > 0;
	}

	int Creature::getLiveCount() const {
		return m_currentLife;
	}

	void Creature::setLiveCount(int lifecount) {
		m_currentLife = lifecount;
	}

	int Creature::getDeadCount() const {
		return m_deadCounter;
	}

	int Creature::getMaxLife() const {
		return m_typeData->m_life;
	}

	unsigned Creature::getSpeed() const {
		return m_typeData->m_speed;
	}

	unsigned Creature::getStrength() const {
		return m_typeData->m_strength;
	}

	const std::string& Creature::getName() const {
		return m_typeData->m_name;
	}

	const std::string& Creature::getCharacteristics() const {
		return m_typeData->m_characteristics;
	}

	const Kachel* Creature::getCurrentTile() const {
		return m_currentTile;
	}

	Kachel* Creature::getCurrentTile() {
		return m_currentTile;
	}

	void Creature::setCurrentTile(Kachel& tile) {
		m_currentTile = &tile;
	}

	CreatureState Creature::getCurrentState() const {
		return m_currentState;
	}

	void Creature::setCurrentState(CreatureState state) {
		m_currentState = state;
	}

	//################################################################################

	std::function<int(Kacheltyp)> Landbewohner::weight_func_land = [](Kacheltyp tile) {
		if (tile == Kacheltyp::STEINE) return 4;
		if (tile == Kacheltyp::SCHNEE) return 2;
		if (tile == Kacheltyp::ERDE || tile == Kacheltyp::SAND) return 1;
		return -1;
	};


	Landbewohner::Landbewohner(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__) {
	}

	bool Landbewohner::canBeOnTerrain(Kacheltyp kacheltyp) const {
		if (kacheltyp != WASSER_S && kacheltyp != WASSER_T)
			return true;
		return false;
	}

	std::vector<std::pair<Kachel*, unsigned>> Landbewohner::findLandPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		//
		if (target.getType() == WASSER_S || target.getType() == WASSER_T)
			return std::vector<std::pair<Kachel*, unsigned>>();
		//
		return AStar(source, target, karte.getMapData(), Landbewohner::weight_func_land);
	}

	bool Landbewohner::isLandbewohner() const {
		return true;
	}

	//###############################################################################

	std::function<int(Kacheltyp)> Wasserbewohner::weight_func_water = [](Kacheltyp tile) {
		if (tile == Kacheltyp::WASSER_T) return 3;
		if (tile == Kacheltyp::WASSER_S) return 1;
		return -1;
	};

	Wasserbewohner::Wasserbewohner(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__) {
	}

	bool Wasserbewohner::canBeOnTerrain(Kacheltyp kacheltyp) const {
		if (kacheltyp == WASSER_S || kacheltyp == WASSER_T)
			return true;
		return false;
	}

	std::vector<std::pair<Kachel*, unsigned>> Wasserbewohner::findWaterPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		//
		if (target.getType() != WASSER_S && target.getType() != WASSER_T)
			return std::vector<std::pair<Kachel*, unsigned>>();
		//
		return AStar(source, target, karte.getMapData(), Wasserbewohner::weight_func_water);
	}

	bool Wasserbewohner::isLandbewohner() const {
		return false;
	}

	//##############################################################################

	Pflanze::Pflanze(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__) {
	}

	bool Pflanze::isPflanze() const {
		return true;
	}

	bool Pflanze::isPflanzenfresser() const {
		return false;
	}

	bool Pflanze::isFleischfresser() const {
		return false;
	}

	//nichtbewachsenes terrain
	bool Pflanze::canPopulateOn(const Kachel& kachel) const {
		if (!canPopulateOnTerrain(kachel.getType()))
			return false;
		for (const std::shared_ptr<Creature>& cr : kachel.getTilecreatures())
			if (cr->isAlive() && cr->isPflanze())
				return false;

		return true;
	}

	void Pflanze::nextAction(LandKarte& terrain_map) {
		switch (m_currentState) {
		case (TOT):
			m_deadCounter++;
			break;
		case (FORTPFLANZEN):
			populate(searchPopulateTiles(terrain_map, 5));
			m_stepsSincePopulate = 0;
			m_currentState = WARTEN;
			break;
		default:
			//default is WARTEN
			unsigned count;
			if (m_stepsSincePopulate > m_currentLife / 100 && (count =
				countNeighborsOfSameKind(terrain_map, 5)) >= 2
				&& count <= 7)
				m_currentState = FORTPFLANZEN;
			else {
				Kacheltyp kt = m_currentTile->getType();
				if (kt == SAND || kt == ERDE || kt == WASSER_S)
					m_currentLife -= 10;
				else
					m_currentLife -= 25;
			}
			break;
		}
		//check if dead
		if (m_currentLife <= 0)
			m_currentState = TOT;
		else
			m_stepsSincePopulate++;
	}

	//#################################################################################

	Tier::Tier(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), m_waitCount(0), m_steps_since_eating(0), m_currentVictim(
			nullptr), m_reachAkku(0) {
		m_currentState = WANDERN;
	}

	bool Tier::isPflanze() const {
		return false;
	}

	//exor function
	bool Tier::isPflanzenfresser() const {
		return !isFleischfresser();
	}

	bool Tier::initMove(const LandKarte& terrain_map,
		const Kachel* target_tile) {
		//check if target valid
		if (target_tile != nullptr) {
			//move assign
			m_targetTilePath = std::move(
				findTargetPath(*m_currentTile, *target_tile, terrain_map));
			//check if path exists
			if (!m_targetTilePath.empty())
				return true;
		}
		return false;
	}

	bool Tier::move(unsigned basereach, unsigned lifedrop) {
		if (m_targetTilePath.size() == 1) {
			//lifedrop
			m_currentLife -= lifedrop;
			m_targetTilePath.clear();
			return true;
		}
		if (m_targetTilePath.size() > 1) {
			//compute target tile within path based on speed
			auto pit = m_targetTilePath.begin();
			while (pit->first != m_currentTile)
				++pit;
			//
			unsigned reach = basereach + pit->second;
			while (pit != m_targetTilePath.end() && pit->second <= reach)
				++pit;
			//move creature to new tile
			std::shared_ptr<Creature>&& cr = m_currentTile->removeCreature(
				this);
			m_currentTile = (--pit)->first;
			m_currentTile->addCreature(std::move(cr));
			//
			m_reachAkku = reach - pit->second;
			//lifedrop
			m_currentLife -= lifedrop;
			//creature reached target
			if (m_currentTile == m_targetTilePath.back().first) {
				m_targetTilePath.clear();
				return true;
			}
		}
		return false;
	}

	void Tier::findRandomPath(const LandKarte& terrain_map, unsigned max_dist) {
		std::mt19937 engine(
			std::chrono::high_resolution_clock::now().time_since_epoch().count());
		std::uniform_int_distribution<unsigned> rand_length(2, max_dist);
		m_targetTilePath.clear();
		int size = rand_length(engine);
		const Kachel* ct = getCurrentTile();
		m_targetTilePath.emplace_back(const_cast<Kachel*>(ct), 0);
		auto& weight_func = (isLandbewohner()) ? Landbewohner::weight_func_land : Wasserbewohner::weight_func_water;
		//
		while (m_targetTilePath.size() < size) {
			auto neighbors = ct->getNeighborRegions();
			for (auto& k : m_targetTilePath) {
				for (auto it = neighbors.begin(); it != neighbors.end();) {
					if (*it == k.first || !canBeOnTerrain((*it)->getType()))
						it = neighbors.erase(it);
					else
						++it;
				}
			}
			if (neighbors.empty())
				break;
			std::uniform_int_distribution<unsigned> rand_index(0, neighbors.size() - 1);
			const Kachel* rand_neighbor = neighbors[rand_index(engine)];
			//way diagonal is sqrt(2) longer
			int deltaX = ct->getX() - rand_neighbor->getX();
			int deltaY = ct->getY() - rand_neighbor->getY();
			int weight = weight_func(rand_neighbor->getType());
			int path_cost = (deltaX != 0 && deltaY != 0) ? weight * 14 : weight * 10;
			int lastcost = m_targetTilePath.back().second;
			m_targetTilePath.emplace_back(const_cast<Kachel*>(rand_neighbor), lastcost + path_cost);
			//
			ct = rand_neighbor;
		}
	}

	bool Tier::chaseVictim() {
		return move(m_reachAkku + getSpeed(), 10);
	}

	bool Tier::walk() {
		return move(m_reachAkku + (getSpeed() / 2), 5);
	}

	void Tier::nextAction(LandKarte& terrain_map) {
#ifdef DEBUG
		std::cout << "CreatureState:" << std::endl;
		std::cout << getName() << ": " << m_currentState
			<< std::endl;
#endif
		switch (m_currentState) {
		case (TOT):
			m_deadCounter++;
			break;
		case (WARTEN):
			m_waitCount =
				(m_currentLife == getMaxLife()) ? 5 : m_waitCount - 1;
			m_currentLife -= 5;
			if (m_waitCount <= 1)
				m_currentState = WANDERN;
			break;
		case (WANDERN):
			unsigned count;
			//gerade auf aktiver Wanderung
			if (!m_targetTilePath.empty()) {
				if (walk())
					m_currentState = WARTEN;
				//else stay in WANDERN
			}
			//paarungsbedingungen erfuellt
			else if (m_stepsSincePopulate > m_currentLife / 50 && (count =
				countNeighborsOfSameKind(terrain_map, 8)) >= 2
				&& count <= 5)
				m_currentState = FORTPFLANZEN;
			// hungrig und jagdbeute gefunden
			else if (m_steps_since_eating > m_currentLife / 80 &&
				(m_currentVictim = findVictim(terrain_map, 10)) != nullptr)
				m_currentState = JAGEN;
			//neue Wanderung starten
			else {
				findRandomPath(terrain_map, 10);
				if (walk())
					m_currentState = WARTEN;
				//else stay in WANDERN
			}
			break;
		case (FORTPFLANZEN):
			populate(searchPopulateTiles(terrain_map, 2));
			m_stepsSincePopulate = 0;
			m_currentState = WANDERN;
			break;
		case (JAGEN):
			if (!m_currentVictim->isAlive())
				m_currentState = WARTEN;
			else if (m_targetTilePath.empty()
				|| m_currentVictim->getCurrentTile()
				!= m_targetTilePath.back().first) {
				if (!initMove(terrain_map,
					m_currentVictim->getCurrentTile()))
					m_currentState = WARTEN;
				else if (chaseVictim())
					m_currentState = ANGRIFF;
				//else stay in JAGEN
			}
			else if (chaseVictim())
				m_currentState = ANGRIFF;
			//else stay in JAGEN
			break;
		case (ANGRIFF):
			if (this->m_currentTile == m_currentVictim->getCurrentTile()) {
				//lifegain
				m_currentLife =
					(m_currentLife + getStrength() / 10 <= getMaxLife()) ?
					m_currentLife + getStrength() / 10 :
					getMaxLife();
				//extern victim lifedrop
				m_currentVictim->setLiveCount(
					m_currentVictim->getLiveCount()
					- this->getStrength());
				if (!m_currentVictim->isAlive()
					|| m_currentLife == getMaxLife()) {
					if (!m_currentVictim->isAlive())
						//extern victim statechange
						m_currentVictim->setCurrentState(TOT);
					m_currentState = WARTEN;
				}
				//else stay in ANGRIFF
			}
			else
				m_currentState = JAGEN;
			m_steps_since_eating = 0;
			break;
		}
		//check if dead
		if (m_currentLife <= 0)
			m_currentState = TOT;
		else {
			m_stepsSincePopulate++;
			m_steps_since_eating++;
		}
	}

	//################################################################################
	Fleischfresser::Fleischfresser(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__) {
	}

	bool Fleischfresser::isFleischfresser() const {
		return true;
	}

	Creature* Fleischfresser::findVictim(const LandKarte& terrain_map,
		unsigned dist) const {
		//generate options vector
		std::vector<Creature*>&& options =
			generic_creature_options(terrain_map, dist,
				[this](const Creature* cr)-> bool {
			return this != cr && cr->isAlive() &&
				cr->isTier() && canBeOnTerrain(cr->getCurrentTile()->getType())
				&& getStrength() * getLiveCount() > cr->getStrength() * cr->getLiveCount();
		});

		//pick closest tile
		if (!options.empty()) {
			Creature* victim = nullptr;
			unsigned min_dist = std::numeric_limits<unsigned>::max();
			for (auto& c : options) {
				unsigned dist = heur_dist_astar(*getCurrentTile(), *c->getCurrentTile());
				if (dist < min_dist) {
					min_dist = dist;
					victim = c;
				}
			}
			return victim;
		}
		return nullptr;
	}

	//###################################################################################

	Pflanzenfresser::Pflanzenfresser(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__) {
	}

	bool Pflanzenfresser::isFleischfresser() const {
		return false;
	}

	Creature* Pflanzenfresser::findVictim(const LandKarte& terrain_map,
		unsigned dist) const {
		//generate options vector
		std::vector<Creature*>&& options = generic_creature_options(terrain_map,
			dist, [this](const Creature* cr)-> bool {
			return cr->isAlive() && cr->isPflanze() &&
				canBeOnTerrain(cr->getCurrentTile()->getType());
		});
#ifdef DEBUG
		std::cout << options.size() << std::endl;
#endif
		//pick closest tile
		if (!options.empty()) {
			Creature* victim = nullptr;
			unsigned min_dist = std::numeric_limits<unsigned>::max();
			for (auto& c : options) {
				unsigned dist = heur_dist_astar(*getCurrentTile(), *c->getCurrentTile());
				if (dist < min_dist) {
					min_dist = dist;
					victim = c;
				}
			}
			return victim;
		}
		return nullptr;
	}

	//Konkrete Klassen
	//######################################################################################
	Algen::Algen(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Algen::getPreferredTerrain() const {
		return WASSER_S;
	}

	void Algen::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Algen>(std::move(kachelOptions));
	}

	//###############################################################################
	Seetang::Seetang(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Seetang::getPreferredTerrain() const {
		return WASSER_S;
	}

	void Seetang::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Seetang>(std::move(kachelOptions));
	}

	//################################################################################
	Plankton::Plankton(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Plankton::getPreferredTerrain() const {
		return WASSER_T;
	}

	void Plankton::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Plankton>(std::move(kachelOptions));
	}

	//#############################################################################
	Gras::Gras(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Gras::getPreferredTerrain() const {
		return ERDE;
	}

	bool Gras::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == ERDE || kacheltyp == STEINE)
			return true;
		return false;
	}

	void Gras::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Gras>(std::move(kachelOptions));
	}

	//#############################################################################
	Kaktus::Kaktus(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Kaktus::getPreferredTerrain() const {
		return SAND;
	}

	bool Kaktus::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == SAND || kacheltyp == ERDE)
			return true;
		return false;
	}

	void Kaktus::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Kaktus>(std::move(kachelOptions));
	}

	//#################################################################################
	Gebuesch::Gebuesch(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Gebuesch::getPreferredTerrain() const {
		return STEINE;
	}

	bool Gebuesch::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == ERDE || kacheltyp == STEINE || kacheltyp == SAND)
			return true;
		return false;
	}

	void Gebuesch::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Gebuesch>(std::move(kachelOptions));
	}

	//###############################################################################
	Sonnenblume::Sonnenblume(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Sonnenblume::getPreferredTerrain() const {
		return ERDE;
	}

	bool Sonnenblume::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == ERDE || kacheltyp == STEINE)
			return true;
		return false;
	}

	void Sonnenblume::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Sonnenblume>(std::move(kachelOptions));
	}

	//###########################################################################
	Eiche::Eiche(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Eiche::getPreferredTerrain() const {
		return ERDE;
	}

	bool Eiche::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == ERDE || kacheltyp == STEINE)
			return true;
		return false;
	}

	void Eiche::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Eiche>(std::move(kachelOptions));
	}

	//###############################################################################
	Obstbaum::Obstbaum(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Obstbaum::getPreferredTerrain() const {
		return ERDE;
	}

	bool Obstbaum::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == ERDE)
			return true;
		return false;
	}

	void Obstbaum::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Obstbaum>(std::move(kachelOptions));
	}

	//################################################################################
	Tannenbaum::Tannenbaum(const KreaturTypData* type_data__,
		Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Pflanze(type_data__,
			current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Tannenbaum::getPreferredTerrain() const {
		return SCHNEE;
	}

	bool Tannenbaum::canPopulateOnTerrain(const Kacheltyp kacheltyp) const {
		if (kacheltyp == ERDE || kacheltyp == STEINE || kacheltyp == SCHNEE)
			return true;
		return false;
	}

	void Tannenbaum::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Tannenbaum>(std::move(kachelOptions));
	}

	//###################################################################################
	Wels::Wels(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Wels::getPreferredTerrain() const {
		return WASSER_S;
	}

	void Wels::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Wels>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Wels::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findWaterPath(source, target, karte);
	}

	//#####################################################################################
	Forelle::Forelle(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Forelle::getPreferredTerrain() const {
		return WASSER_S;
	}

	void Forelle::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Forelle>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Forelle::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findWaterPath(source, target, karte);
	}

	//#################################################################
	Krabbe::Krabbe(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Krabbe::getPreferredTerrain() const {
		return WASSER_S;
	}

	void Krabbe::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Krabbe>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Krabbe::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findWaterPath(source, target, karte);
	}

	//##################################################################
	Hai::Hai(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Fleischfresser(type_data__,
				current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Hai::getPreferredTerrain() const {
		return WASSER_T;
	}

	void Hai::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Hai>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Hai::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findWaterPath(source, target, karte);
	}

	//###########################################################################################
	Delphin::Delphin(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Fleischfresser(type_data__,
				current_tile__), Wasserbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Delphin::getPreferredTerrain() const {
		return WASSER_T;
	}

	void Delphin::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Delphin>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Delphin::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findWaterPath(source, target, karte);
	}

	//##########################################################################################
	Kuh::Kuh(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Kuh::getPreferredTerrain() const {
		return ERDE;
	}

	void Kuh::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Kuh>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Kuh::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//################################################################
	Pferd::Pferd(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Pferd::getPreferredTerrain() const {
		return ERDE;
	}

	void Pferd::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Pferd>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Pferd::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//##################################################################
	Emu::Emu(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Emu::getPreferredTerrain() const {
		return SAND;
	}

	void Emu::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Emu>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Emu::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//###############################################################################
	Schaf::Schaf(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Pflanzenfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Schaf::getPreferredTerrain() const {
		return ERDE;
	}

	void Schaf::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Schaf>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Schaf::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//########################################################################
	Hund::Hund(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Fleischfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Hund::getPreferredTerrain() const {
		return ERDE;
	}

	void Hund::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Hund>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Hund::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//########################################################################
	Baer::Baer(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Fleischfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Baer::getPreferredTerrain() const {
		return SCHNEE;
	}

	void Baer::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Baer>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Baer::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//#############################################################
	Tiger::Tiger(const KreaturTypData* type_data__, Kachel* current_tile__) :
		Creature(type_data__, current_tile__), Tier(type_data__,
			current_tile__), Fleischfresser(type_data__,
				current_tile__), Landbewohner(type_data__, current_tile__) {
	}

	Kacheltyp Tiger::getPreferredTerrain() const {
		return STEINE;
	}

	void Tiger::populate(std::vector<Kachel*>&& kachelOptions) const {
		Creature::generic_populate<Tiger>(std::move(kachelOptions));
	}

	std::vector<std::pair<Kachel*, unsigned>> Tiger::findTargetPath(
		const Kachel& source, const Kachel& target,
		const LandKarte& karte) const {
		return findLandPath(source, target, karte);
	}

	//####################################################################

}/*namespace biosim*/
