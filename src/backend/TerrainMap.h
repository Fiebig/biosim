/*
 * TerrainMap.h
 *
 *  Created on: 13.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_TERRAINMAP_H_
#define BACKEND_TERRAINMAP_H_

#include <vector>

namespace biosim {

	enum Direction
		:unsigned {
			NORTH, NORTH_EAST, EAST, SOUTH_EAST,
		SOUTH, SOUTH_WEST, WEST, NORTH_WEST
	};

	class Kachel;

	class LandKarte {
		unsigned m_width, m_height;
		std::vector<Kachel> m_mapData;
	public:
		LandKarte(unsigned width, unsigned height);
		const Kachel& operator()(unsigned x, unsigned y) const;
		Kachel& operator()(unsigned x, unsigned y);
		const Kachel& at(unsigned x, unsigned y) const;
		const Kachel* get_tile_neighbor(const Kachel& tile, Direction direction) const;
		std::vector<const Kachel*> get_all_tile_neighbors(const Kachel& tile) const;
		const std::vector<Kachel>& getMapData() const;
		std::vector<Kachel>& getMapData();
		void clearMap();
        int isLeft(const Kachel& kachel, const Kachel& a, const Kachel& b) const;
		unsigned getWidth() const;
		unsigned getHeight() const;
	private:
		void init_tile_neighbors();
		void generate_map();
	};
}/*namespace biosim*/

#endif /* BACKEND_TERRAINMAP_H_ */
