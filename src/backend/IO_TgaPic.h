/*
 * IO_TgaPic.h
 *
 *  Created on: 06.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_IO_TGAPIC_H_
#define BACKEND_IO_TGAPIC_H_

#include <memory>

namespace biosim {

	struct RGBA {
		//little-endian Reihenfolge
		unsigned char B, G, R, A = 255;
	};

	struct TgaHeader {
		unsigned char bildID, farbpalettentyp, bildtyp;
		unsigned short palettenbeginn, palettenlaenge;
		unsigned char groessepalletteneintrag;
		unsigned short xnull, ynull, bildbreite, bildhoehe;
		unsigned char bitpropixel, bildattrbyte;
	};

	/**
	 * @class TgaPicture
	 *
	 */
	class TgaPicture {
		TgaHeader m_header;
		std::unique_ptr<unsigned char[]> m_rawdata;
		TgaPicture(const TgaHeader& header_, unsigned char* data_);
		TgaPicture(const TgaHeader& header_,
				std::unique_ptr<unsigned char[]> data_);
	public:
		TgaPicture(const TgaPicture& other) = delete;
		TgaPicture(TgaPicture&& other) noexcept = default;
		TgaPicture& operator=(const TgaPicture& other) = delete;
		TgaPicture& operator=(TgaPicture&& other) noexcept = default;
		RGBA operator()(unsigned x, unsigned y) const;
		RGBA at(unsigned x, unsigned y) const;
		const TgaHeader& getHeader() const;
		const unsigned char* getRawData() const;
		unsigned width() const;
		unsigned height() const;
		static TgaPicture load(const char* filepath);
	private:
		static TgaHeader readHeader(const unsigned char* headerData);
	};

}/*namespace biosim*/

#endif /* BACKEND_IO_TGAPIC_H_ */
