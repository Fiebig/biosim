/*
 * PerlinNoise.h
 *
 *  Created on: 12.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_PERLINNOISE_H_
#define BACKEND_PERLINNOISE_H_

#include <vector>

namespace biosim {

	class PerlinNoise {
		// The permutation vector
		std::vector<unsigned> m_p;
		// The gradient vectors
		std::vector<double> m_gx;
		std::vector<double> m_gy;
	public:
		// Generate a new perlin noise based on the value of seed
		PerlinNoise(unsigned seed, unsigned size = 256);
		// Fractional Bownian Moment
		double fBm(double x, double y, double amplitude = 1.0,
				double frequency = 1.0, unsigned octaves = 1,
				double lacunarity = 2.0, double gain = 0.5) const;
		// Get a noise value for (x,y)
		double noise(double x, double y) const;
	};

}/*namespace biosim*/
#endif /* BACKEND_PERLINNOISE_H_ */
