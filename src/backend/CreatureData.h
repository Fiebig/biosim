/*
 * CrearureData.h
 *
 *  Created on: 05.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_CREATUREDATA_H_
#define BACKEND_CREATUREDATA_H_

#include <string>

namespace biosim {

	class KreaturTypData {
	public:
		std::string m_name;
		unsigned m_strength, m_speed, m_life;
		std::string m_characteristics, m_picturePath;
	};

}/*namespace biosim*/

#endif /* BACKEND_CREATUREDATA_H_ */
