/*
 * AStar.cpp
 *
 *  Created on: 18.08.2015
 *      Author: Alex
 */
#include "AStar.h"

#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <limits>

#include <custom_utils/DirectUpdateablePQ.h>
#include "Tile.h"

namespace biosim {

	typedef std::unordered_map<const Kachel*, const Kachel*> PREDMAP;
	typedef std::unordered_map<const Kachel*, unsigned> COSTMAP;
	typedef custom_utils::DirectUpdateableMinPQ<const Kachel*, unsigned> OPENSET;
	typedef std::unordered_set<const Kachel*> CLOSEDSET;
	typedef std::function<int(Kacheltyp)> WEIGHTFUNC;

	// heuristic function; must not overestimate the real distance!!!
	unsigned heur_dist_astar(const Kachel& current, const Kachel& target) {
		unsigned xDistance = std::abs(
			(int)current.getX() - (int)target.getX());
		unsigned yDistance = std::abs(
			(int)current.getY() - (int)target.getY());
		if (xDistance > yDistance)
			return 14 * yDistance + 10 * (xDistance - yDistance);
		else
			return 14 * xDistance + 10 * (yDistance - xDistance);
	}

	// returns path from source to target with nodecosts in order
	static PATHVECTOR reconstructPath(const PREDMAP& pred,
	                                  const COSTMAP& g_costs, const Kachel& target) {
		PATHVECTOR res;
		const Kachel* current = &target;
		unsigned pathcost;
		while (current != nullptr) {
			pathcost = g_costs.at(current);
			res.emplace_back(const_cast<Kachel*>(current), pathcost);
			current = pred.at(current);
		}
		std::reverse(res.begin(), res.end());
		return res;
	}

	static void expandNode(const Kachel* current, const Kachel& target,
	                       OPENSET& pq_open, CLOSEDSET& closed, COSTMAP& g_costs,
	                       PREDMAP& pred, WEIGHTFUNC wfunc) {
		//foreach neighbor of current
		for (const Kachel* neighbor : current->getNeighborRegions()) {
			// get weight from weighttable table (minimum 1)
			int weight = wfunc(neighbor->getType());
			// neighbor is hindrance
			if (weight <= 0)
				continue;
			// neighbor is in closedset
			auto clnit = closed.find(neighbor);
			if (clnit != closed.end())
				continue;
			//way diagonal is sqrt(2) longer
			int deltaX = current->getX() - neighbor->getX();
			int deltaY = current->getY() - neighbor->getY();
			weight = (deltaX != 0 && deltaY != 0) ? weight * 14 : weight * 10;
			// calculate new distance
			unsigned cost_found = g_costs.at(current) + weight;
			bool n_in_open = pq_open.contains(neighbor);
			// neighbor not in openset or found better distance
			if (!n_in_open || cost_found < g_costs.at(neighbor)) {
				pred.emplace(neighbor, current);
				//update g
				g_costs.emplace(neighbor, cost_found);
				// update f = g + h
				unsigned f_cost = cost_found + heur_dist_astar(*current, target);
				if (n_in_open)
					pq_open.update_key(neighbor, f_cost);
				else
					pq_open.insert(neighbor, f_cost);
			}
		}
	}

	PATHVECTOR AStar(const Kachel& source, const Kachel& target,
	                 const std::vector<Kachel>& nodes, WEIGHTFUNC wfunc,
	                 unsigned max_nodes) {
		//init
		int bucket_count = 2 * max_nodes;
		COSTMAP g_costs(bucket_count);
		g_costs.emplace(&source, 0);
		PREDMAP pred(bucket_count);
		pred.emplace(&source, nullptr);
		OPENSET pq_open(bucket_count);
		//PQ needs f-values
		pq_open.insert(&source, heur_dist_astar(source, target));
		CLOSEDSET closed(bucket_count);
		//main loop
		while (!pq_open.empty()) {
			const Kachel* current = pq_open.extract_min();
			//found target; return shortest path to it
			if (current == &target)
				return reconstructPath(pred, g_costs, target);
			//expand current node
			expandNode(current, target, pq_open, closed, g_costs, pred, wfunc);
			//current is finshed
			closed.insert(current);
			// abort search if too many nodes have already been finished
			if (closed.size() > max_nodes)
				return PATHVECTOR();
		}
		//return empty vector on failure
		return PATHVECTOR();
	}

}/*namespace biosim*/
