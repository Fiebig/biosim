/*
 * PerlinNoise.cpp
 *
 *  Created on: 13.08.2015
 *      Author: Alex
 */

#include "PerlinNoise.h"

#include <numeric>
#include <cmath>
#include <random>
#include <algorithm>

namespace biosim {

	PerlinNoise::PerlinNoise(unsigned seed, unsigned size) :
			m_p(size), m_gx(size), m_gy(size) {
		// Fill p with values from 0 to size
		std::iota(m_p.begin(), m_p.end(), 0);
		// Initialize a random engine with seed
		std::mt19937 engine(seed);
		// Suffle  using the above random engine
		std::shuffle(m_p.begin(), m_p.end(), engine);
		// initialize gradients
		std::uniform_real_distribution<double> distrib(-1, 1);
		for (unsigned i = 0; i < size; ++i) {
			m_gx[i] = distrib(engine);
			m_gy[i] = distrib(engine);
		}
	}

	double PerlinNoise::fBm(double x, double y, double amplitude,
			double frequency, unsigned octaves, double lacunarity,
			double gain) const {
		double sum = 0.0;
		double total_amplitude = 0.0;
		for (unsigned i = 0; i < octaves; i++) {
			total_amplitude += amplitude;
			sum += amplitude * noise(x * frequency, y * frequency);
			amplitude *= gain;
			frequency *= lacunarity;
		}
		//normalisation
		sum /= total_amplitude;
		return sum;
	}

	double PerlinNoise::noise(double x, double y) const {
		// Compute the integer positions of the four surrounding points
		int qx0 = (int) std::floor(x);
		int qx1 = qx0 + 1;
		int qy0 = (int) std::floor(y);
		int qy1 = qy0 + 1;
		// Permutate values to get indices to use with the gradient look-up tables
		unsigned size = m_p.size();
		unsigned q00 = m_p[(qy0 + m_p[qx0 % size]) % size];
		unsigned q01 = m_p[(qy0 + m_p[qx1 % size]) % size];
		unsigned q10 = m_p[(qy1 + m_p[qx0 % size]) % size];
		unsigned q11 = m_p[(qy1 + m_p[qx1 % size]) % size];
		// Computing vectors from the four points to the input point
		double tx0 = x - std::floor(x);
		double tx1 = tx0 - 1;
		double ty0 = y - std::floor(y);
		double ty1 = ty0 - 1;
		// Compute the dot-product between the vectors and the gradients
		double v00 = m_gx[q00] * tx0 + m_gy[q00] * ty0;
		double v01 = m_gx[q01] * tx1 + m_gy[q01] * ty0;
		double v10 = m_gx[q10] * tx0 + m_gy[q10] * ty1;
		double v11 = m_gx[q11] * tx1 + m_gy[q11] * ty1;
		// Do the bi-cubic interpolation to get the final value
		double wx = (3 - 2 * tx0) * tx0 * tx0;
		double v0 = v00 - wx * (v00 - v01);
		double v1 = v10 - wx * (v10 - v11);
		double wy = (3 - 2 * ty0) * ty0 * ty0;
		double v = v0 - wy * (v0 - v1);
		return v;
	}

}/*namespace biosim*/
