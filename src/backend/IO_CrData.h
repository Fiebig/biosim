/*
 * IO_CrData.h
 *
 *  Created on: 05.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_IO_CRDATA_H_
#define BACKEND_IO_CRDATA_H_

#include <vector>

namespace biosim {

	class KreaturTypData;

	/**
	 * Liest eine Textdatei mit biologischen Daten verschiedener Kreaturtypen ein.
	 * Jeder Eintrag umfasst eine Zeile. Die einzelnen Daten (Felder) eines Kreaturtyps
	 * sind durch einen Komma getrennt. Vor dem ersten Feld und nach dem letzten Feld
	 * steht kein Komma. Es kann leere Felder geben (erkennbar durch zwei
	 * aufeinanderfolgende Komma oder einen Komma am Anfang bzw. Ende der Zeile).
	 * Sind Fehler in einer Zeile, so wird diese Zeile ignoriert.
	 *
	 * @param filepath - Pfad zur Textdatei
	 * @param columsPerLine - Anzahl Spalten pro Zeile
	 * @throws std::ios_base::failure - Falls badbit gesetzt oder Fehler beim Oeffnen auftritt
	 * @return Vector mit den verschiedenen Kreaturentypen nach Name alphabetisch sortiert
	 */
	std::vector<KreaturTypData> readCreatureTypes(const char* filepath,
		unsigned columsPerLine = 6);

}/*namespace biosim*/

#endif /* BACKEND_IO_CRDATA_H_ */
