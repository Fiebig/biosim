/*
 * IO_CrData.cpp
 *
 *  Created on: 05.08.2015
 *      Author: Alex
 */
#include "IO_CrData.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#ifdef WIN32
#include <sys\stat.h>
#else //for Unix etc
#include <sys/stat.h>
#include <sys/types.h>
#endif

#include "CustomExceptions.h"
#include "CreatureData.h"

namespace biosim {

	static bool createCreature(std::vector<KreaturTypData>& creatures,
			const std::vector<std::string>& fields, unsigned lineNr) {
		KreaturTypData cr;
		if ((cr.m_name = fields[0]).empty()) {
			std::cerr << "Zeile " << lineNr << ": Fehlender Kreaturenname"
					<< std::endl;
			return false;
		}
		char* endptr;
		cr.m_strength = strtoul(fields[1].c_str(), &endptr, 10);
		if (*endptr != 0) {
			std::cerr << "Zeile " << lineNr << ": Ung�ltige Staerke"
					<< std::endl;
			return false;
		}
		cr.m_speed = strtoul(fields[2].c_str(), &endptr, 10);
		if (*endptr != 0) {
			std::cerr << "Zeile " << lineNr << ": Ung�ltige Geschwindigkeit"
					<< std::endl;
			return false;
		}
		cr.m_life = strtoul(fields[3].c_str(), &endptr, 10);
		if (*endptr != 0) {
			std::cerr << "Zeile " << lineNr << ": Ung�ltige Lebensdauer"
					<< std::endl;
			return false;
		}
		//Eigenschaften optional
		cr.m_characteristics = fields[4];
		cr.m_picturePath = fields[5];
		//newline character entfernen!!!???
		if (cr.m_picturePath.back() == '\n' || cr.m_picturePath.back() == '\r')
			cr.m_picturePath.pop_back();
		struct stat buf;
		if (stat(cr.m_picturePath.c_str(), &buf) != 0
				|| !(buf.st_mode & S_IFREG)) {
			std::cerr << "Zeile " << lineNr << ": Ung�ltiger Bildpfad"
					<< std::endl;
			return false;
		}
		creatures.push_back(cr);
		return true;
	}

	static bool readLine(std::vector<KreaturTypData>& creatures,
			const std::string& line, unsigned lineNr, unsigned columsPerLine) {
		std::istringstream ss(line);
		std::vector<std::string> fields(columsPerLine);
		for (unsigned i = 0; i < columsPerLine; ++i)
			if (!std::getline(ss, fields[i], ',')) {
				std::cerr << "Zeile " << lineNr << ": Spaltenfehler"
						<< std::endl;
				return false;
			}
		if (!ss.eof()) {
			std::cerr << "Zeile " << lineNr << ": Spaltenfehler" << std::endl;
			return false;
		}
		if (!createCreature(creatures, fields, lineNr))
			return false;
		return true;
	}

	std::vector<KreaturTypData> readCreatureTypes(const char* filepath,
			unsigned columsPerLine) {
		std::ifstream file;
		file.exceptions(std::ios::badbit);
		file.open(filepath, std::ios::in | std::ios::binary);
		if (!file) {
			std::string msg("Fehler beim Oeffnen der Datei ");
			msg.append(filepath);
			throw std::ios_base::failure(msg);
		}
		std::vector<KreaturTypData> res;
		std::string line;
		unsigned lineNr = 0, anzKorrekt = 0;
		while (std::getline(file, line))
			if (readLine(res, line, ++lineNr, columsPerLine))
				anzKorrekt++;
		file.close();
		if (res.empty())
			throw InvalidResource(
					"Datei enthaelt keine gueltige Kreaturendefinition");
#ifdef DEBUG
		std::cout << lineNr << " Zeilen gelesen, davon " << anzKorrekt
				<< " Korrekte und " << (lineNr - anzKorrekt) << " Ungueltige."
				<< std::endl;
#endif
		//Kreaturentypen nach name sortieren
		std::sort(res.begin(), res.end(),
				[](const KreaturTypData& a, const KreaturTypData& b) -> bool
				{
					return a.m_name < b.m_name;
				});
		return res;
	}

}/*namespace biosim*/
