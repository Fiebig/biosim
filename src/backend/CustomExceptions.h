/*
 * CustomExceptions.h
 *
 *  Created on: 13.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_CUSTOMEXCEPTIONS_H_
#define BACKEND_CUSTOMEXCEPTIONS_H_

#include <stdexcept>

namespace biosim {

	class InvalidResource: public std::logic_error {
	public:
		InvalidResource(const std::string &message_) :
				std::logic_error(message_) {
		}
	};

}/*namespace biosim*/

#endif /*BACKEND_CUSTOMEXCEPTIONS_H_*/
