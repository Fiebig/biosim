/*
 * Tile.h
 *
 *  Created on: 13.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_TILE_H_
#define BACKEND_TILE_H_

#include <memory>
#include <list>
#include <vector>

namespace biosim {

	class Creature;

	enum Kacheltyp
	:unsigned {
		WASSER_T = 0,
		WASSER_S,
		SAND,
		ERDE,
		STEINE,
		SCHNEE
	};

	class Kachel {
		//LandKarte initialisiert alle Kacheln!
		friend class LandKarte;
		unsigned m_x, m_y;
		Kacheltyp m_type;
		std::list<std::shared_ptr<Creature>> m_tileCreatures;
		//for AStar
		std::vector<const Kachel*> m_neighborRegions;
	public:
		Kachel(unsigned x__ = 0, unsigned y__ = 0, Kacheltyp type__ = ERDE);
		Kacheltyp getType() const;
		unsigned getX() const;
		unsigned getY() const;
		unsigned creatureCount() const;
		const std::list<std::shared_ptr<Creature>>& getTilecreatures() const;
		const std::vector<const Kachel*>& getNeighborRegions() const;
		void addCreature(std::shared_ptr<Creature>&& cr);
		void addCreature(const std::shared_ptr<Creature>& cr);
		std::shared_ptr<Creature> removeCreature(const Creature* cr);
	};

}/*namespace biosim*/

#endif /* BACKEND_TILE_H_ */
