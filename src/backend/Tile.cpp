/*
 * Tile.cpp
 *
 *  Created on: 17.08.2015
 *      Author: Alex
 */

#include "Tile.h"
#include "Creature.h"

namespace biosim {

	Kachel::Kachel(unsigned x__, unsigned y__, Kacheltyp type__) :
			m_x(x__), m_y(y__), m_type(type__) {
	}

	Kacheltyp Kachel::getType() const {
		return m_type;
	}

	unsigned Kachel::getX() const {
		return m_x;
	}

	unsigned Kachel::getY() const {
		return m_y;
	}

	unsigned Kachel::creatureCount() const {
		return m_tileCreatures.size();
	}

	const std::vector<const Kachel*>& Kachel::getNeighborRegions() const {
		return m_neighborRegions;
	}

	const std::list<std::shared_ptr<Creature>>& Kachel::getTilecreatures() const {
		return m_tileCreatures;
	}

	void Kachel::addCreature(std::shared_ptr<Creature>&& cr) {
		m_tileCreatures.push_back(std::move(cr));
	}

	void Kachel::addCreature(const std::shared_ptr<Creature>& cr) {
		m_tileCreatures.push_back(cr);
	}

	std::shared_ptr<Creature> Kachel::removeCreature(const Creature* cr) {
		std::shared_ptr<Creature> toremove;
		for (auto it = m_tileCreatures.begin(); it != m_tileCreatures.end();
				++it)
			if (it->get() == cr) {
				toremove = std::move(*it);
				m_tileCreatures.erase(it);
				break;
			}
		return toremove;
	}

}/*namespace biosim*/
