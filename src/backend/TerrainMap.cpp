/*
 * TerrainMap.cpp
 *
 *  Created on: 13.08.2015
 *      Author: Alex
 */
#include "TerrainMap.h"

#include <string>
#include <algorithm>
#include <chrono>
#include <stdexcept>
#include <cmath>

#include "PerlinNoise.h"
#include "Tile.h"

namespace biosim {

	LandKarte::LandKarte(unsigned width_, unsigned height_) :
			m_width(width_), m_height(height_), m_mapData(width_ * height_) {
		generate_map();
		init_tile_neighbors();
	}

	const Kachel& LandKarte::operator()(unsigned x, unsigned y) const {
		return m_mapData[y * m_width + x];
	}

	Kachel& LandKarte::operator()(unsigned x, unsigned y) {
		return m_mapData[y * m_width + x];
	}

	const Kachel& LandKarte::at(unsigned x, unsigned y) const {
		if (x >= m_width || y >= m_height)
			throw std::out_of_range(
					'(' + std::to_string(x) + ',' + std::to_string(y)
							+ ") au�erhalb der Landkarte");
		return m_mapData[y * m_width + x];
	}

	const Kachel* LandKarte::get_tile_neighbor(const Kachel& tile,
			Direction direction) const {
		unsigned neighbor_x = tile.getX(), neighbor_y = tile.getY();
		switch (direction) {
			case NORTH:
				neighbor_y--;
				break;
			case NORTH_EAST:
				neighbor_y--;
				neighbor_x++;
				break;
			case EAST:
				neighbor_x++;
				break;
			case SOUTH_EAST:
				neighbor_y++;
				neighbor_x++;
				break;
			case SOUTH:
				neighbor_y++;
				break;
			case SOUTH_WEST:
				neighbor_y++;
				neighbor_x--;
				break;
			case WEST:
				neighbor_x--;
				break;
			case NORTH_WEST:
				neighbor_y--;
				neighbor_x--;
				break;
		}
		if (neighbor_x < 0 || neighbor_y < 0 || neighbor_x >= m_width
				|| neighbor_y >= m_height)
			return nullptr;
		return &m_mapData[neighbor_y * m_width + neighbor_x];
	}

	std::vector<const Kachel*> LandKarte::get_all_tile_neighbors(
			const Kachel& tile) const{
		int t_x = tile.getX();
		int t_y = tile.getY();
		std::vector<const Kachel*> res;
		for (int i = t_y - 1; i <= t_y + 1; i++) {
			if (i >= 0 && i < m_height)
				for (int j = t_x - 1; j <= t_x + 1; j++) {
					if (j >= 0 && j < m_width && (i != t_y || j != t_x))
						res.push_back(&m_mapData[i * m_width + j]);
				}
		}
		return res;
	}

	const std::vector<Kachel>& LandKarte::getMapData() const {
		return m_mapData;
	}

	std::vector<Kachel>& LandKarte::getMapData() {
		return m_mapData;
	}

	unsigned LandKarte::getWidth() const {
		return m_width;
	}

	unsigned LandKarte::getHeight() const {
		return m_height;
	}

    int LandKarte::isLeft(const Kachel& kachel, const Kachel& a, const Kachel& b) const {
        int ax = a.getX(), ay = a.getY();
        int bx = b.getX(), by = b.getY();
        int cx = kachel.getX(), cy = kachel.getY();
        return (bx - ax)*(cy - ay) - (by - ay)*(cx - ax);
    }

	void LandKarte::clearMap() {
		for (Kachel& k : m_mapData)
			k.m_tileCreatures.clear();
	}

	void LandKarte::init_tile_neighbors() {
		for (Kachel& k : m_mapData) {
			k.m_neighborRegions = get_all_tile_neighbors(k);
		}
	}

	void LandKarte::generate_map() {
		PerlinNoise pn(
				std::chrono::high_resolution_clock::now().time_since_epoch().count());
		for (unsigned i = 0; i < m_height; i++)
			for (unsigned j = 0; j < m_width; j++) {
				//map [0..width] [0..1]
				double x = j / ((double) m_width);
				//map [0..height] [0..1]
				double y = i / ((double) m_height);
				//compute noise frequency
				double frequency = std::max(
						std::floor(
								std::log(std::min(m_width, m_height))
										/ std::log(1.7)) - 3, 1.0);
				//compute noise [-0.5...0.5]
				double noiseValue = pn.fBm(x, y, 1.0, frequency, 8);
				//scale to [-500...500]
				int n = (int) (noiseValue * 1000);
				// map value to landscape type
				Kacheltyp k_type;
				if (n > 200)
					k_type = WASSER_T;
				else if (n > 75)
					k_type = WASSER_S;
				else if (n > 0)
					k_type = SAND;
				else if (n < -200)
					k_type = SCHNEE;
				else if (n < -100)
					k_type = STEINE;
				else
					k_type = ERDE;
				Kachel& k = m_mapData[i * m_width + j];
				k.m_x = j;
				k.m_y = i;
				k.m_type = k_type;
			}
	}

}/*namespace biosim*/
