/*
 * Creature.h
 *
 *  Created on: 16.08.2015
 *      Author: Alex
 */

#pragma once
#ifndef BACKEND_CREATURE_H_
#define BACKEND_CREATURE_H_

#ifdef _MSC_VER
#pragma warning(disable:4250)
#endif

#include <memory>
#include <list>
#include <vector>
#include <functional>

#include "CreatureData.h"

namespace biosim {

	class LandKarte;
	class Kachel;
	enum Kacheltyp
	:unsigned;

	/**
	 * Abstrakte Basisklassen:
	 *
	 *               _________Creature__________________________
	 * 				|	         |		       |		        |
	 * 	     Landbewohner  Wasserbewohner	Pflanze     _______Tier_________
	 * 												   |		  		    |
	 * 										    Pflanzenfresser	      Fleischfresser
	 */

	//Abstrakte Klassen
	//####################################################################################
	enum CreatureState
	:unsigned {
		WARTEN = 0,
		FORTPFLANZEN,
		TOT,
		WANDERN,
		JAGEN,
		ANGRIFF
	};

	class Creature {
	public:
		// fuer schnelle traversierung aller kreatureninstanzen u.a. fuer KI
		static std::list<std::shared_ptr<Creature>> s_allCreatures;
		//no ownership
		const KreaturTypData* m_typeData;
	protected:
		//lifecounter
		int m_currentLife;
		//no ownership
		Kachel* m_currentTile;
		//state information
		CreatureState m_currentState;
		//fuer fortpflanzung
		int m_stepsSincePopulate;
		//fuer dead.tga darstellung
		int m_deadCounter;
	public:
		Creature(const KreaturTypData* type_data__, Kachel* current_tile__);
		virtual ~Creature() noexcept = default;
		//pure virtual functions
		virtual bool isLandbewohner() const = 0;
		virtual bool isPflanze() const = 0;
		virtual bool isPflanzenfresser() const = 0;
		virtual bool isFleischfresser() const = 0;
		virtual Kacheltyp getPreferredTerrain() const = 0;
		virtual bool canBeOnTerrain(Kacheltyp kacheltyp) const = 0;
		//creature makes action based on its state and the state of the environment
		virtual void nextAction(LandKarte& terrain_map) = 0;
		//NON virtual functions
		//exor functions
		bool isWasserbewohner() const;
		bool isTier() const;
		//getter + setter functions
		bool isAlive() const;
		int getLiveCount() const;
		void setLiveCount(int lifecount);
		int getDeadCount() const;
		int getMaxLife() const;
		unsigned getSpeed() const;
		unsigned getStrength() const;
		const std::string& getName() const;
		const std::string& getCharacteristics() const;
		const Kachel* getCurrentTile() const;
		Kachel* getCurrentTile();
		void setCurrentTile(Kachel& tile);
		CreatureState getCurrentState() const;
		void setCurrentState(CreatureState state);
	protected:
		//pure virtual functions
		virtual void populate(std::vector<Kachel*>&& kachelOptions) const = 0;
		//overrideable functions
		virtual bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const;
		virtual bool canPopulateOn(const Kachel& kachel) const;
		//NON virtual functions
		unsigned countNeighborsOfSameKind(const LandKarte& terrain_map,
		                                  unsigned dist) const;
		std::vector<Kachel*> searchPopulateTiles(const LandKarte& terrain_map,
		                                         unsigned dist) const;
		//template functions for local use (cannot be virtual)
		template <typename T>
		void generic_populate(std::vector<Kachel*>&& kachelOptions) const;
		template <typename Func>
		std::vector<Kachel*> generic_kachel_options(
			const LandKarte& terrain_map, unsigned dist,
			Func&& boolfunc) const;
		template <typename Func>
		std::vector<Creature*> generic_creature_options(
			const LandKarte& terrain_map, unsigned dist,
			Func&& boolfunc) const;
	};

	//#####################################################################################

	class Landbewohner: public virtual Creature {
	public:
		static std::function<int(Kacheltyp)> weight_func_land;

		Landbewohner(const KreaturTypData* type_data__, Kachel* current_tile__);
		virtual ~Landbewohner() noexcept = default;
		virtual bool canBeOnTerrain(Kacheltyp kacheltyp) const override;
		bool isLandbewohner() const override final;
	protected:
		virtual std::vector<std::pair<Kachel*, unsigned>> findLandPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const;
	};

	//###############################################################################

	class Wasserbewohner: public virtual Creature {
	public:
		static std::function<int(Kacheltyp)> weight_func_water;

		Wasserbewohner(const KreaturTypData* type_data__,
		               Kachel* current_tile__);
		virtual ~Wasserbewohner() noexcept = default;
		virtual bool canBeOnTerrain(Kacheltyp kacheltyp) const override;
		bool isLandbewohner() const override final;
	protected:
		virtual std::vector<std::pair<Kachel*, unsigned>> findWaterPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const;
	};

	//################################################################################

	class Pflanze: public virtual Creature {
	public:
		Pflanze(const KreaturTypData* type_data__, Kachel* current_tile__);
		virtual ~Pflanze() noexcept = default;
		bool isPflanze() const override final;
		//pflanze frisst keine anderen kreaturen
		bool isPflanzenfresser() const override final;
		bool isFleischfresser() const override final;
		//plants behave differently than animals
		virtual void nextAction(LandKarte& terrain_map) override;
	protected:
		virtual bool canPopulateOn(const Kachel& kachel) const override;
	};

	//###############################################################################
	class Tier: public virtual Creature {
	protected:
		int m_waitCount, m_steps_since_eating;
		Creature* m_currentVictim;
		std::vector<std::pair<Kachel*, unsigned>> m_targetTilePath;
		unsigned m_reachAkku;
	public:
		Tier(const KreaturTypData* type_data__, Kachel* current_tile__);
		virtual ~Tier() noexcept = default;
		bool isPflanze() const override final;
		//exor function
		bool isPflanzenfresser() const override final;
		//animals behave differently than plants
		virtual void nextAction(LandKarte& terrain_map) override;
	protected:
		// pure virtual functions
		virtual Creature* findVictim(const LandKarte& terrain_map,
		                             unsigned dist) const = 0;
		virtual std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const = 0;
		//overrideable functions
		virtual bool chaseVictim();
		virtual bool walk();
		virtual bool move(unsigned reachbase, unsigned lifedrop);
		virtual bool initMove(const LandKarte& terrain_map,
		                      const Kachel* target_tile);
		virtual void findRandomPath(const LandKarte& terrain_map,
		                                 unsigned dist);
	};

	//##########################################################################################
	class Fleischfresser: public virtual Tier {
	public:
		Fleischfresser(const KreaturTypData* type_data__,
		               Kachel* current_tile__);
		virtual ~Fleischfresser() noexcept = default;
		bool isFleischfresser() const override final;
	protected:
		virtual Creature* findVictim(const LandKarte& terrain_map,
		                             unsigned dist) const override;
	};

	//###########################################################################################
	class Pflanzenfresser: public virtual Tier {
	public:
		Pflanzenfresser(const KreaturTypData* type_data__,
		                Kachel* current_tile__);
		virtual ~Pflanzenfresser() noexcept = default;
		bool isFleischfresser() const override final;
	protected:
		virtual Creature* findVictim(const LandKarte& terrain_map,
		                             unsigned dist) const override;
	};

	//###########################################################################################
	//Konkrete Klassen (die verschiedenen Kreaturtypen aus der Eingabedatei)
	//###########################################################################################

	class Algen final: public Pflanze, public Wasserbewohner {
	public:
		Algen(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
	};

	//############################################################################

	class Seetang final: public Pflanze, public Wasserbewohner {
	public:
		Seetang(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
	};

	//############################################################################

	class Plankton final: public Pflanze, public Wasserbewohner {
	public:
		Plankton(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
	};

	//############################################################################

	class Gras final: public Pflanze, public Landbewohner {
	public:
		Gras(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Kaktus final: public Pflanze, public Landbewohner {
	public:
		Kaktus(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Gebuesch final: public Pflanze, public Landbewohner {
	public:
		Gebuesch(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Sonnenblume final: public Pflanze, public Landbewohner {
	public:
		Sonnenblume(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Eiche final: public Pflanze, public Landbewohner {
	public:
		Eiche(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Obstbaum final: public Pflanze, public Landbewohner {
	public:
		Obstbaum(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Tannenbaum final: public Pflanze, public Landbewohner {
	public:
		Tannenbaum(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		bool canPopulateOnTerrain(const Kacheltyp kacheltyp) const override;
	};

	//############################################################################

	class Wels final: public Pflanzenfresser, public Wasserbewohner {
	public:
		Wels(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Forelle final: public Pflanzenfresser, public Wasserbewohner {
	public:
		Forelle(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Krabbe final: public Pflanzenfresser, public Wasserbewohner {
	public:
		Krabbe(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Hai final: public Fleischfresser, public Wasserbewohner {
	public:
		Hai(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Delphin final: public Fleischfresser, public Wasserbewohner {
	public:
		Delphin(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Kuh final: public Pflanzenfresser, public Landbewohner {
	public:
		Kuh(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Pferd final: public Pflanzenfresser, public Landbewohner {
	public:
		Pferd(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Emu final: public Pflanzenfresser, public Landbewohner {
	public:
		Emu(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Schaf final: public Pflanzenfresser, public Landbewohner {
	public:
		Schaf(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Hund final: public Fleischfresser, public Landbewohner {
	public:
		Hund(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Baer final: public Fleischfresser, public Landbewohner {
	public:
		Baer(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

	class Tiger final: public Fleischfresser, public Landbewohner {
	public:
		Tiger(const KreaturTypData* type_data__, Kachel* current_tile__);
		Kacheltyp getPreferredTerrain() const override;
	protected:
		void populate(std::vector<Kachel*>&& kachelOptions) const override;
		std::vector<std::pair<Kachel*, unsigned>> findTargetPath(
			const Kachel& source, const Kachel& target,
			const LandKarte& karte) const override;
	};

	//############################################################################

}/*namespace biosim*/

#endif /* BACKEND_CREATURE_H_ */
