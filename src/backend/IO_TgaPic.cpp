/*
 * IO_TgaPic.cpp
 *
 *  Created on: 06.08.2015
 *      Author: Alex
 */
#include "IO_TgaPic.h"

#include <string>
#include <fstream>
#include <stdexcept>
#include <cstring>

#include "CustomExceptions.h"

namespace biosim {
	TgaPicture::TgaPicture(const TgaHeader& header__, unsigned char* data_) :
		m_header(header__), m_rawdata(data_) {
	}

	TgaPicture::TgaPicture(const TgaHeader& header__,
	                       std::unique_ptr<unsigned char[]> data_) :
		m_header(header__), m_rawdata(std::move(data_)) {
	}

	RGBA TgaPicture::operator()(unsigned x, unsigned y) const {
		RGBA res;
		if (m_header.bitpropixel == 32)
			memcpy(&res.B, m_rawdata.get() + 4 * (y * m_header.bildbreite + x),
			       4);
		else
			memcpy(&res.B, m_rawdata.get() + 3 * (y * m_header.bildbreite + x),
			       3);
		return res;
	}

	RGBA TgaPicture::at(unsigned x, unsigned y) const {
		if (x >= m_header.bildbreite || y >= m_header.bildhoehe)
			throw std::out_of_range(
				'(' + std::to_string(x) + ',' + std::to_string(y)
				+ ") au�erhalb des Bildes");
		RGBA res;
		if (m_header.bitpropixel == 32)
			memcpy(&res.B, m_rawdata.get() + 4 * (y * m_header.bildbreite + x),
			       4);
		else
			memcpy(&res.B, m_rawdata.get() + 3 * (y * m_header.bildbreite + x),
			       3);
		return res;
	}

	const TgaHeader& TgaPicture::getHeader() const {
		return m_header;
	}

	const unsigned char* TgaPicture::getRawData() const {
		return m_rawdata.get();
	}

	unsigned TgaPicture::width() const {
		return m_header.bildbreite;
	}

	unsigned TgaPicture::height() const {
		return m_header.bildhoehe;
	}

	TgaPicture TgaPicture::load(const char* filepath) {
		//init stream
		std::ifstream file;
		file.exceptions(std::ios::badbit | std::ios::failbit);
		file.open(filepath, std::ios::in | std::ios::binary);
		//read and check header
		unsigned char headerData[18];
		file.read(reinterpret_cast<char*>(headerData), sizeof(headerData));
		const TgaHeader& header = TgaPicture::readHeader(headerData);
		//read BGR(A) picture data
		unsigned anzbyte;
		if (header.bitpropixel == 32)
			anzbyte = header.bildbreite * header.bildhoehe * 4;
		else
			anzbyte = header.bildbreite * header.bildhoehe * 3;
		std::unique_ptr<unsigned char[]> databuf(new unsigned char[anzbyte]);
		file.read(reinterpret_cast<char*>(databuf.get()), anzbyte);
		file.close();
		return TgaPicture(header, std::move(databuf));
	}

	TgaHeader TgaPicture::readHeader(const unsigned char* headerData) {
		TgaHeader header;
		if ((header.bildID = headerData[0]) != 0)
			throw InvalidResource("Ungueltiges Bildformat: BildID");
		if ((header.farbpalettentyp = headerData[1]) != 0)
			throw InvalidResource("Ungueltiges Bildformat: Farbpallettentyp");
		if ((header.bildtyp = headerData[2]) != 2)
			throw InvalidResource("Ungueltiges Bildformat: Bildtyp");
		// little endian !!!
		header.palettenbeginn = (headerData[4] << 8) | headerData[3];
		if (header.palettenbeginn != 0)
			throw InvalidResource("Ungueltiges Bildformat: Palettenbeginn");
		header.palettenlaenge = (headerData[6] << 8) | headerData[5];
		if (header.palettenlaenge != 0)
			throw InvalidResource("Ungueltiges Bildformat: Palettenlaenge");
		header.groessepalletteneintrag = headerData[7];
		header.xnull = (headerData[9] << 8) | headerData[8];
		if (header.xnull != 0)
			throw InvalidResource("Ungueltiges Bildformat: Xnull");
		header.ynull = (headerData[11] << 8) | headerData[10];
		if (header.ynull != 0)
			throw InvalidResource("Ungueltiges Bildformat: Ynull");
		header.bildbreite = (headerData[13] << 8) | headerData[12];
		header.bildhoehe = (headerData[15] << 8) | headerData[14];
		header.bitpropixel = headerData[16];
		if (header.bitpropixel != 24 && header.bitpropixel != 32)
			throw InvalidResource("Ungueltiges Bildformat: Bitpropixel");
		header.bildattrbyte = headerData[17];
		return header;
	}
}/*namespace biosim*/
